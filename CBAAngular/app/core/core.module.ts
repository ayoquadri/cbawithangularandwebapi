import  { NgModule } from "@angular/core";
import  { BrowserModule } from "@angular/platform-browser";
import  { HttpModule } from "@angular/http";    
import { AuthService,AuthGuard,AuthHttp,ToastService} from "../shared/index";

@NgModule({
    providers:[AuthService,AuthGuard,AuthHttp,ToastService]
})
export class CoreModule{}