import  { NgModule, ModuleWithProviders } from "@angular/core";

import  { CommonModule } from "@angular/common";

import  { ReactiveFormsModule, FormsModule } from "@angular/forms";

import  { RouterModule } from "@angular/router";
import { Ng2Bs3ModalModule } from 'ng2-bs3-modal/ng2-bs3-modal';

import { AuthService, AuthHttp, SideBarComponent, MyReportTemplateComponent, MyTemplateComponent ,
             SpinnerComponent, AuthGuard, NavButtonComponent, NotFoundComponent, DetailViewFooterComponent, 
                NavHeaderComponent, PaginationComponent , TableOptionComponent, BackButtonComponent,
                ToastService,SearchComponent} from "./";

@NgModule({
    declarations:[

        MyReportTemplateComponent,  MyTemplateComponent,    SpinnerComponent,   SideBarComponent,   NavButtonComponent, 
        NotFoundComponent, DetailViewFooterComponent, NavHeaderComponent , 
        PaginationComponent, TableOptionComponent, BackButtonComponent,SearchComponent
    ],
    

    imports : [

         ReactiveFormsModule, FormsModule, CommonModule, RouterModule
    ],
    exports:[

        ReactiveFormsModule,    CommonModule,  RouterModule,  MyReportTemplateComponent,  MyTemplateComponent,
        SpinnerComponent,   SideBarComponent, NavButtonComponent, NotFoundComponent, DetailViewFooterComponent, 
        NavHeaderComponent, PaginationComponent,TableOptionComponent, BackButtonComponent,Ng2Bs3ModalModule,SearchComponent
    ]

    
    
})
export class SharedModule{}

    // static forRoot(): ModuleWithProviders{
    //     return {

    //         ngModule:SharedModule,
    //         providers : [AuthService, AuthHttp , AuthGuard, ToastService]

    //     }
    // }   
