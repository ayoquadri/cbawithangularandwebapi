 import { Component,AfterViewInit,Inject } from '@angular/core';
 import { Router } from '@angular/router';
 import { AuthService } from '../';
declare var $:any;
@Component({
moduleId : module.id,
selector: 'my-template',
templateUrl: 'myTemplate.component.html',
})
export class MyTemplateComponent {

    constructor(private router: Router, private authService: AuthService){
        
    }

    signOut(){
        this.authService.logOut();
        this.router.navigate(["/Login"]);
    }
}
