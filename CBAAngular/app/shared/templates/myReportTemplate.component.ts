 import { Component,AfterViewInit } from '@angular/core';
 import { Router } from '@angular/router';
  import { AuthService,ToastService} from '../index';

declare var $:any;
@Component({
  moduleId : module.id,
  selector: 'my-report-template',
  templateUrl: 'myReportTemplate.component.html',
  //providers:[AuthService]
})
export class MyReportTemplateComponent {

  constructor(private router: Router, private authService: AuthService){
        
    }
  
    signOut(){
        this.authService.logOut();
        this.router.navigate(["/Login"]);
    }
  
 }
