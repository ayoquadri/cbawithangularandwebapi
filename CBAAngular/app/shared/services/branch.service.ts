import { Injectable } from '@angular/core';
  import {Response, Headers} from '@angular/http';
import {Observable} from 'rxjs/Observable';

import {AuthHttp} from './auth.service';
import {IBranch} from '../models/Branch';


@Injectable()
export class BranchService{
    headers:Headers
    
    url ="http://localhost:58801/api/branches";
    constructor(private _authHttp:AuthHttp){

    }
    // https://angular.io/docs/ts/latest/guide/server-communication.html#!#fetch-data
    getBranches(page:number, pageSize:number){ 
      
      return this._authHttp
             .get( this.url + "/" + pageSize + "/" + page, { headers:this.headers})
            // .map((res:Response)=> <IBranch[]> res.json())
             //.catch(this.handleError)

              // return this._http
              //             .get( `${this.url} / ${pageSize} / ${page}`, { headers:this.headers})
             //.map( res => res.json());  
          //.catch(this.handleError);
   }
   
   getBranchById(id:number){
     return this._authHttp
             .get( this.url + "/" + id)
             .map(res=> res.json());
   }

   saveBranch(branch){

     return this._authHttp
                .post(this.url,JSON.stringify(branch), { headers:this.headers})
                //.map(res =>res..json());
   }
   updateBranch(id:number,branch){
     //this.headers.append("Content-Type","application/json");;
     return this._authHttp
                .put(this.url+ "/"+ id,JSON.stringify(branch), {headers:this.headers})
                //.map(res =>res..json());
   }
  
  private handleError(error:Response){
    //TODO: send the error to remote logging infrastructure
    console.error(error);
    return Observable.throw(error);
  }
   
   
  
}