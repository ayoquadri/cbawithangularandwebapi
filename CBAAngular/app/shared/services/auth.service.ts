import {Injectable,Inject} from "@angular/core";
import {Http,Headers,URLSearchParams,Response,RequestOptionsArgs,Request,RequestMethod,RequestOptions} 
from "@angular/http";
import {Router} from "@angular/router";
import {Observable} from "rxjs/Observable"
import {Subscription} from "rxjs/Subscription"
import "rxjs/add/operator/catch"

import {TOKEN_URL, TOKEN_NAME, AUTH_HEADER_NAME, AUTH_HEADER_PREFIX} from "../";
//import {Injectable} from "Storage";


@Injectable()
export class AuthService {
     
     private _isLoggedIn:boolean = false;
     //private TOKEN_URL: string = "http://localhost:58801/token";
     private header = new Headers();
     //private TOKEN_NAME = "auth_token";
     public redirectUrl:string = null;
     private subscription:Subscription
    
     constructor(private _http: Http, private router:Router) {
         debugger;
       this._isLoggedIn = !!localStorage.getItem(TOKEN_NAME);
     }

     isLoggedIn():boolean {

        //  localStorage.removeItem(TOKEN_NAME);
        //  alert(localStorage.getItem(TOKEN_NAME));
         return localStorage.getItem(TOKEN_NAME) != null;
        // return false;
         //return !!localStorage.getItem(TOKEN_NAME)
         //return this._isLoggedIn;
     }

     getToken(): string {
         //if(!this.isLoggedIn()) return null;

         return localStorage.getItem(TOKEN_NAME);
     }

     login(username:string,password:string) : Observable<Response> {

         this.header.append("Content-type","application/x-www-form-urlencoded; charset=UTF-8");
         let params = new URLSearchParams();
         params.append("grant_type","password");
         params.append("username",username);
         params.append("password",password);

         return this._http
            .post(TOKEN_URL,params.toString())
            
     }

     logOut():void{

         localStorage.removeItem(TOKEN_NAME);
         this._isLoggedIn = false;

     }
}

export interface ITokenInfo{
    
    access_token:string;
    token_type : string;
    expires_in :number;
    username :  string;
}

@Injectable()
export class AuthHttp{

    constructor(private _http:Http, private _authService: AuthService, private router: Router){
        debugger;
    }

    private request(url:string | Request, options?: RequestOptionsArgs): Observable<Response>{

        if (typeof url === 'string') {

            return this.get(url, options); // Recursion: transform url from String to Request
        }


        if(!this._authService.isLoggedIn()){

            return new Observable<Response>((obs:any)=>{

                obs.error = new Error("User is not authenticated");
            })
        }

        // from this point url is always an instance of Request;
        let req:Request = <Request>url;

        req.headers.set(AUTH_HEADER_NAME, AUTH_HEADER_PREFIX + ` ${this._authService.getToken()}`);
        req.headers.append("Content-Type","application/json");
        req.headers.append("Accept","application/json");

        //return this._http.request(req).catch(new ErrorHandler(this.router).handleError);
        return this._http.request(req).catch((error:Response)=>
                    {
                        if(error.status == 401){
                            this._authService.logOut();
                            this.router.navigate(["Login"]);
                        }
                        return Observable.throw(error)
                    });

    }

    private requestHelper(requestArgs:RequestOptionsArgs): Observable<Response>{
        let options = new RequestOptions(requestArgs);

        return this.request(new Request(options));

    }

    get(url:string, options?:RequestOptionsArgs) : Observable<Response>{
        return this.requestHelper({url:url,method:RequestMethod.Get});
    }

    post(url:string, body:string, options?:RequestOptionsArgs) : Observable<Response>{
        return this.requestHelper({url:url,body:body, method:RequestMethod.Post});
    }

    put(url:string, body:string, options?:RequestOptionsArgs) : Observable<Response>{
        return this.requestHelper({url:url,body:body, method:RequestMethod.Put});
    }

    delete(url:string, body:string, options?:RequestOptionsArgs) : Observable<Response>{
        return this.requestHelper({url:url,method:RequestMethod.Delete});
    }

    patch(url:string, body:string, options?:RequestOptionsArgs) : Observable<Response>{
        return this.requestHelper({url:url,body:body, method:RequestMethod.Patch});
    }
    head(url:string, body:string, options?:RequestOptionsArgs) : Observable<Response>{
        return this.requestHelper({url:url,body:body, method:RequestMethod.Head});
    }


    // private handleError(error:Response,@Inject(Router) router?){
    // //TODO: send the error to remote logging infrastructure
    //     debugger;
    //     if(error.status == 401)
    //         ErrorHandler.routeToLogin();    
    //         //router.navigate(["Login"]);
    //     return Observable.throw(error);
    // }
    
  
}
@Injectable()
class ErrorHandler{
    
     constructor(private router: Router) {
        this.router.navigate(["Login"]);
     }
    handleError(error:Response){
   
    }
   
}

// export const AUTH_PROVIDERS: any = [
//   provide(AuthHttp, {
//     useFactory: (http: Http) => {
//       return new AuthHttp(http);
//     },
//     deps: [Http]
//   })
// ];

