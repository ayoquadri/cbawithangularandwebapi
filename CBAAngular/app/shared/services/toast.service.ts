
import {Injectable} from "@angular/core";
declare var $:any;

@Injectable()
export class ToastService{
    
    toastOptions :IToastOptions;
    /**
     *
     */
    constructor() {
        this.toastOptions = {
                            heading:'',
                            text:"", showHideTransition: 'fade', allowToastClose:true, stack:false,
                            icon:'', position : 'top-center',
                            hideAfter: 5000, loader:false
                        };
    }

    showSuccessToast(message){
        this.toastOptions.heading = message;
        this.toastOptions.icon = "success";
        $.toast(this.toastOptions)
    }

}

export class IToastOptions{
    heading:string;
    text:string;
    showHideTransition:string;
    allowToastClose:boolean;
    stack:boolean;
    icon:string;
    position:string;
    hideAfter:number;
    loader:boolean;
}