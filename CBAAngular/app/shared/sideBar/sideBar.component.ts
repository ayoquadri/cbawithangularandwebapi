import {Component,OnInit, ViewChild,ElementRef} from '@angular/core';
import {FormControl} from '@angular/forms';
import {Http} from '@angular/http';
import "rxjs/add/operator/map";
import "rxjs/add/operator/debounceTime";
import "rxjs/add/operator/distinctUntilChanged";
declare var $:any;

@Component({
    moduleId:module.id,
    selector: 'my-side-bar',
    templateUrl: 'sideBarTemplate.html'
})
export class SideBarComponent implements OnInit{
   menus = [];
   filteredMenus;
   @ViewChild("sideBar") sideBar:ElementRef;
   term = new FormControl();
   constructor(private _http:Http){
       //this.sideBar.nativeElement
       this.term.valueChanges
                .debounceTime(400)
                .distinctUntilChanged()
                .subscribe(
                    term=>
                    { 
                        this.filterSideBar(term);
                        //setTimeout(this.setUpJquery,200);
                        //this.setUpJquery();
                    }
                );  
   }
   
   ngOnInit(){
       this._http
            .get("user_menus/menus.json")
            .map(response=>response.json())
            .subscribe(
                x=>this.filteredMenus= this.menus=x,
                ()=> alert("could not load menu file")
                    //()=> setTimeout(this.setUpJquery,200 )
            );
            
                 
   }
   
   filterSideBar(term){
       this.filteredMenus = [];
       //var menu:any;
       for(var i=0; i < this.menus.length;i++){
           
           var subMenus = this.menus[i].subMenus;
           
           if(subMenus){
               //debugger;
               var filteredMenu = $.extend(true, {}, this.menus[i]);
               filteredMenu.subMenus =[]
               var subMenusMatched = [];
               for (var j=0; j < subMenus.length;j++) { 
                   
                    if(subMenus[j].name.toLowerCase().indexOf(term) > -1){
                       subMenusMatched.push(subMenus[j]);
                       }
               }
               if(subMenusMatched.length>0){
                   filteredMenu.subMenus= subMenusMatched;
                   this.filteredMenus.push(filteredMenu);
               }
                 
           }
           else{
               if(this.menus[i].name.toLowerCase().indexOf(term) > -1)
                    this.filteredMenus.push(this.menus[i]);
           }
        }
        //this.setUpJquery();
        //window.setTimeout(this.setUpJquery,200);
   }
   
   ngAfterContentInit(){
   }

   setUpJquery(){
       console.log("setUpJquery");
   }

 }  