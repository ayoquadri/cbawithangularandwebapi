import {FormControl} from '@angular/forms';
export class GlobalValidator{

    static validateEmail(control: FormControl){

        var EMAIL_REGEXP = 

        /^[a-z](?:\w|\.)*@((?:[a-z0-9]+[-.]?[a-z0-9]+)+)(?:\.[a-z]+)$/i;

        if ( !EMAIL_REGEXP.test(control.value))
        {
            return { "incorrectMailFormat": true };
        }

        return null;
    }

    static validatePhoneNumber(control:FormControl){
        var PHONE_REGEX = /^(?:\+234|0){1}[7-9]\d[1-9]\d{7}$/;
        if(!PHONE_REGEX.test(control.value))
            return {"invalidPhoneNumber":true}
        
        return null;

    }
}