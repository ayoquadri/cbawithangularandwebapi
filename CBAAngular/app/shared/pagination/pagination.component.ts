import {Component,OnInit,Input,Output,OnChanges,EventEmitter} 
            from '@angular/core';
//import {} from 'angular2/common';

@Component({
    moduleId: module.id,
    selector: "pagination",
    templateUrl:"pagination.component.html",
    styles:[`
        .pagination li:hover {
            background-color: #ecf0f1;
            border-color : #ecf0f1;
            color :#2c3e50;
            
        }
        .pagination {
            margin: 0px;
        }
    `]
})
export class PaginationComponent implements OnChanges{
    
    @Input() items= [];
    @Input() totalCount = 0;
    @Input("page-size") pageSize:number=0; 
    @Input("max-page-links") maxPageLinks; 

    @Output("page-changed") pageChanged = new EventEmitter();

    totalNumberOfItems;
    numberOfPages;
    pagesNumbers = [];
    currentPage=1;
    indexofFirstItemInPage;
    indexofLastItemInPage;
    
    get firstItemInPage(){
        return (this.currentPage-1 )* this.pageSize + 1;
    }
    get lastItemInPage(){

        let lastItemInPage = Math.min((this.currentPage-1) * this.pageSize + this.pageSize, this.totalCount);

       return lastItemInPage;
    }
    
    ngOnChanges(changes){
        //this.totalNumberOfItems = this.items.length;
        this.totalNumberOfItems = this.totalCount;
        debugger;
        if(this.totalNumberOfItems==0){
            
            this.numberOfPages=0;
            //this.currentPage = undefined;
            // this.indexofFirstItemInPage = undefined;
            // this.indexofLastItemInPage = undefined;
        }
        else{
            
            if(this.pageSize > 0){
                //round of to upper whole number
                this.numberOfPages = 
                        Math.ceil(this.totalNumberOfItems/this.pageSize);
            }
            else{
                
                this.numberOfPages = 1;
            }
           
            if(this.numberOfPages > 1){
                
                this.pagesNumbers = [];
                var length = this.numberOfPages;
                for (var index = 1; index <= length; index++) {
                    this.pagesNumbers.push(index);
                }
     
            }
            
            this.currentPage = 1;
           // this.processPageChanged();
        }
        
       // this.raiseEvent();
            
    }
    
    pageClicked(pageNo){
        
        if(pageNo == this.currentPage) return;

        this.currentPage = pageNo;
       // this.processPageChanged();
        this.raiseEvent();
      
    }
    //NOte isFirst and Last is not being set
    gotoPreviousPage(){
        if(this.currentPage == 1)
            return;
        this.currentPage--;
        this.raiseEvent();
        //this.processPageChanged();
    }
    
    gotoNextPage(){
        if(this.currentPage == this.numberOfPages)
            return;

        this.currentPage++;
        this.raiseEvent();
        //this.processPageChanged();
    }
    
    processPageChanged(){
        debugger;
        this.indexofFirstItemInPage =
            ((this.pageSize* this.currentPage)- this.pageSize);
         
        var excessPages = 0;   
        if(this.totalNumberOfItems < this.pageSize*this.currentPage)
        {
            excessPages = (this.pageSize*this.currentPage) 
                          - this.totalNumberOfItems; 
        }
        
        this.indexofLastItemInPage =  
            (this.pageSize*this.currentPage) - 1 - excessPages;
        //this.currentPage = pageNo;
        
        
    }
    
    raiseEvent(){
        
        this.pageChanged.emit(
        {   
            page: this.currentPage
        });  
    }
}