export const BASE_API_URL = "http://localhost:58801";
export const TOKEN_URL = BASE_API_URL + "/token";
export const TOKEN_NAME = "auth_token";
export const AUTH_HEADER_NAME = "Authorization";
export const AUTH_HEADER_PREFIX = "Bearer";