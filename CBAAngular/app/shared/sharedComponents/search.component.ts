import { Component, OnInit,Input, 
    Output, EventEmitter} from '@angular/core';
import { FormBuilder, FormGroup,FormControl} from '@angular/forms';
@Component({
    moduleId: module.id,
    selector: 'search',
    templateUrl: 'search.component.html'
})
export class SearchComponent implements OnInit {
    
    @Input() isSearching=false;
    @Output() onSearch = new EventEmitter();
    searchTextBox= new FormControl();

    constructor() {
        this.searchTextBox.valueChanges
                            .debounceTime(400)
                            .distinctUntilChanged()
                            .filter(term => term.length > 2 || term.length == 0)
                            .do((term)=>{
                                this.isSearching = true
                                this.onSearch.emit(term);
                            }).subscribe();
     }

    ngOnInit() { }

    onSearchButtonClicked(){
        var searchTerm = this.searchTextBox.value;
        if(searchTerm=="")
            return;
        this.isSearching = true;
        this.onSearch.emit(searchTerm);
    }

}