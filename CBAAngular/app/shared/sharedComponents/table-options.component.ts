import { Component,Input,Output,EventEmitter } from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'table-option',
    templateUrl: 'table-options.component.html',
})   

export class TableOptionComponent {

    @Output("page-size-changed") pageSizeChangedEvent = new EventEmitter();


    pageSizeChanged(selPageSize){

        this.pageSizeChangedEvent.emit({
            pageSize : selPageSize
        });
     
    }



}