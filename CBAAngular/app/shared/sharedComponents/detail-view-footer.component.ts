import { Component, OnInit,Input,Output, EventEmitter } from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'detail-view-footer',
    templateUrl: 'detail-view-footer.component.html'
})
export class DetailViewFooterComponent implements OnInit {

    @Input() editPageLink= [];
    @Input() reportPageLink= [];
    @Output() editButtonClicked = new EventEmitter()

    ngOnInit() { }

    onEditButtonClicked(){
        this.editButtonClicked.emit({});
    }

}