import { Component, OnInit, Output, EventEmitter} from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'nav-header',
    templateUrl: 'nav-header.component.html'
})
export class NavHeaderComponent{

   @Output("sign-out") signOut = new EventEmitter();

   SignOutButtonClicked(){
       this.signOut.emit();
   }

}