import { Component, OnInit,Input, Output, EventEmitter} from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'back-button',
    templateUrl: 'back-button.component.html'
})
export class BackButtonComponent implements OnInit {
    
    @Input() buttonText;
    @Input() isDetailPage = true;
    @Output() backButtonClicked = new EventEmitter();
    constructor() { }

    ngOnInit() { }

    onButtonClicked(){
        this.backButtonClicked.emit({});
    }

}