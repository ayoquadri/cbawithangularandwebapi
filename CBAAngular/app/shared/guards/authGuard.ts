import { Component,AfterViewInit ,Injectable} from '@angular/core';
import {CanActivate,
                ActivatedRouteSnapshot,RouterStateSnapshot,Router } from '@angular/router';
import { AuthService } from '../services/index';
//import { AddBranchComponent } from './branchManagement/addBranch.component';

@Injectable()
export class AuthGuard implements CanActivate {

    constructor(private _authService: AuthService,private _router:Router){

    }

    canActivate(route:ActivatedRouteSnapshot, state:RouterStateSnapshot):boolean{

        if(this._authService.isLoggedIn() == true) return true;

        console.log("Activated route component");
        console.log(route.component)
        

        this._authService.redirectUrl =  state.url;

        this._router.navigate(["Login"]);
    }
}