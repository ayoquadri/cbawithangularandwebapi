
export interface PagedResponse<T>{
    total:number;
    data: T[];
}

// export interface Data<E>{
//     total:number;
//     data: E;
// }
