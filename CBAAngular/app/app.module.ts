import  { NgModule } from "@angular/core";
import  { BrowserModule } from "@angular/platform-browser";
import  { HttpModule } from "@angular/http";
import  { RouterModule } from "@angular/router";
import { AppComponent } from "./app.component";
//import  { ReactiveFormsModule,FormsModule } from "@angular/forms";
// import { BranchModule,LoginModule, HomePageModule, UserModule, 
//     CustomerModule,CustomerAccountModule} from "./main/index";
import {HomePageModule} from "./main/index";
import { SharedModule } from "./shared/shared.module";
import { CoreModule } from "./core/core.module";
//import { AuthService,AuthHttp,AuthGuard,ToastService} from "./shared/index";

import { AppRoutingModule,routing } from "./app-routing.module"

@NgModule({
    declarations:[AppComponent],
    imports : [
        BrowserModule, HttpModule ,HomePageModule,CoreModule,AppRoutingModule
    ],
    //providers:[ToastService],
    bootstrap: [AppComponent]
})
export class AppModule{}