import { Component,AfterViewInit,AfterContentInit,AfterViewChecked} from '@angular/core';
import { SideBarComponent } from './shared/index';
import {AuthService} from './shared/services/index';

//import { AddBranchComponent } from './branchManagement/addBranch.component';
declare var $:any;
@Component({
    moduleId:module.id,
    selector: 'my-app',
    templateUrl: 'app.component.html',
    //providers:[AuthService]
})
export class AppComponent implements AfterViewInit{
    currentYear:Number
    disable:boolean = false;
    
    constructor(){
        this.currentYear = new Date().getFullYear();
    }
    
    ngAfterViewInit(){

        $.AdminLTE.layout.activate();
       
    }
  
    //NEVER NEVER DO AN AFFTER VIEW CHECK
    // ngAfterViewChecked(){
    //     console.log("AfterViewChecked");
    //     $.AdminLTE.layout.activate();
    // }
 }
