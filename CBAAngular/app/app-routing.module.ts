import {NgModule} from "@angular/core";
import { Routes,
         RouterModule } from '@angular/router';

//import {LoginComponent} from "./main/login/index"
import { NotFoundComponent } from "./shared";

const routes: Routes = [
  
  { path: '', redirectTo:"Home" ,pathMatch:"full"},
  //{ path: '', component:AboutComponent,pathMatch:"full"},

 { path: 'Home', loadChildren:"app/main/Home/home-page.module#HomePageModule"},

 // { path: 'Home', component:HomePageComponent , canActivate: [AuthGuard]},

   { path: 'Login', loadChildren:"app/main/Login/login.module#LoginModule"},
  { path: 'UserManagement', loadChildren:"app/main/UserMgmt/user.module#UserModule"},
  { path: 'BranchManagement', loadChildren:"app/main/BranchManagement/branch.module#BranchModule"},
  { path: 'CustomerManagement', loadChildren:"app/main/CustomerMgmt/customer.module#CustomerModule"},
  { path: 'CustomerAccountManagement', 
                  loadChildren:"app/main/CustomerAccountMgmt/customerAccount.module#CustomerAccountModule"},

  { path: '**', component: NotFoundComponent}
];

@NgModule({
  imports:[RouterModule.forRoot(routes, {useHash:true})],
  exports: [RouterModule]
})
export class AppRoutingModule{}
export const routing = RouterModule.forRoot(routes);