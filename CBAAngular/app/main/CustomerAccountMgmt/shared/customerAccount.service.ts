import { Injectable } from '@angular/core';
import { Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import "rxjs/add/operator/do";

import { AuthHttp } from '../../../shared/index';
//import { Branch } from '../shared/index';


@Injectable()
export class CustomerAccountService{
    headers:Headers
    
    url ="http://localhost:58801/api/customerAccounts";

    constructor(private _authHttp:AuthHttp){

    }

    getCustomerAccounts(page:number, pageSize:number,searchTerm?:string){ 
    
      searchTerm = searchTerm? searchTerm:"";
      var searchUrl = this.url + `?searchTerm=${searchTerm}&$skip=${(page-1)*pageSize}&$top=${pageSize}&$inlinecount=allpages`;
     
       return this._authHttp
                  .get(searchUrl);       
   }
   
   getCustomerAccountById(id:string){
     return this._authHttp
             .get( this.url + "/" + id)
             .map(res=> res.json());
   }

   saveCustomerAccount(customerAcc){
     return this._authHttp
                .post(this.url
                    ,JSON.stringify(customerAcc), { headers:this.headers});
   }
   updateCustomerAccount(id,user){
     return this._authHttp
                .put(this.url+ "/"+ id,JSON.stringify(user), {headers:this.headers})
   }
  
  private handleError(error:Response){
    //TODO: send the error to remote logging infrastructure
    console.error(error);
    return Observable.throw(error);
  }
   
   
  
}