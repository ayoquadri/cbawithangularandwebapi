
export class CustomerAccount{
   // id :string = "";
    id :number;
    name:string;
    customerId:number;
    customerCustomerId:number;
    customerFirstName:string;
    customerLastName:string;
    customerOtherNames:string;
    accountType:string="";
    isAccountActive:boolean;
    balance:number;
    branchId:string="";
    branchName:string;
    accountNumber:number;
}