import {Injectable } from '@angular/core';
import { Router, ActivatedRouteSnapshot, Resolve} from '@angular/router';
import {Observable} from "rxjs/Observable";
import "rxjs/add/operator/catch";

import {CustomerAccount} from "./customerAccount.model";
import {CustomerAccountService} from "./customerAccount.service";

@Injectable()
export class CustomerAccountResolve implements Resolve<CustomerAccount> {

   constructor(private customerAccountService:CustomerAccountService, private router: Router){

   }

   resolve(route:ActivatedRouteSnapshot){

       debugger;
       //let id = +route.params["id"];
       let id = route.params["id"];


        return this
                .customerAccountService.getCustomerAccountById(id)
                .catch(()=> this.router.navigate(["CustomerAccountManagement"]));//should return only one branch
            // .subscribe(
            //     branch =>{
            //         return false;
            //     },
            //     (errorResponse)=>{
            //         this.router.navigate(["BranchManagement"]);
            //         return false;
            //     }
            // );
   }

}