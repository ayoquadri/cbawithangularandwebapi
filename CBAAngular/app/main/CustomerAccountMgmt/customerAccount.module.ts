import  { NgModule,CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
 import  { CustomerAccountService, CustomerAccountResolve } from "./shared/index";
 import  { AddCustomerAccountComponent,CustomerAccountDetailsComponent,CustomerAccountListComponent } from "./index";
 import  { BranchService } from "../BranchManagement/index";
 import  { CustomerService } from "../CustomerMgmt/index";
import  { CustomerAccountRoutingModule } from "./customerAccount-routing.module";
import { SharedModule } from "../../shared/shared.module";

@NgModule({
   declarations:[AddCustomerAccountComponent, CustomerAccountListComponent,
   CustomerAccountDetailsComponent],
    imports : [
        SharedModule,
     
        CustomerAccountRoutingModule
    ],
    providers:[CustomerAccountService,BranchService,CustomerService],
    
    schemas:[CUSTOM_ELEMENTS_SCHEMA]
})
export class CustomerAccountModule{}