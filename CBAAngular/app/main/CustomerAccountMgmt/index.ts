// export * from "./addBranch/";

export * from "./customerAccount-add/addCustomerAccount.component";
export * from "./customerAccount-details/customerAccountDetails.component";
export * from "./customerAccount-list/customerAccountList.component";

export * from "./shared/index";

