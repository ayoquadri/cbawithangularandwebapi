import { Component,OnInit,OnDestroy } from '@angular/core';
import { Router, ActivatedRoute} from '@angular/router';
import { Observable } from "rxjs/Observable";

import { CustomerAccountService, CustomerAccount } from '../shared/index';
declare var $:any;

@Component({
    moduleId :module.id,
    templateUrl: 'customerAccountDetails.component.html',
})

export class CustomerAccountDetailsComponent  implements OnInit,OnDestroy{
    private customerAcc:CustomerAccount;
    private name:string;
    isLoading:boolean = false;
    failedToLoad = false;
    notFound = false;
    private subscription:any;

    constructor(private customerAccService:CustomerAccountService,private route: ActivatedRoute,private router:Router) {
       
       this.customerAcc = new CustomerAccount();
    }
    ngOnInit(){

         this.route.data.forEach((data:{customerAcc:CustomerAccount}) =>{
            
            if(data.customerAcc){
                this.customerAcc = data.customerAcc;
            }
         });
    }
    gotoEditPage($event?){
        this.router.navigate(["CustomerAccountManagement/Edit",this.customerAcc.id]);
    }    

     gotoReportPage($event?){
          this.router.navigate(["CustomerAccountManagement"]);
     } 
    ngOnDestroy(){
       // this.subscription.unsubscribe();
    }
}



