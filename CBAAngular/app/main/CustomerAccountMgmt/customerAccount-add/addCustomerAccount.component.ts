import { Component,AfterContentInit,OnInit,Input,OnDestroy,AfterViewInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators,FormControl} from '@angular/forms';
import { Router, ActivatedRoute} from '@angular/router';

import { CustomerAccount, CustomerAccountService} from '../shared/index';
import {GlobalValidator,ToastService} from '../../../shared/index';
import  { BranchService,Branch } from "../../BranchManagement/index";
import  { CustomerService,Customer } from "../../CustomerMgmt/index";
import "rxjs/add/operator/switchMap";
import "rxjs/add/operator/filter";

declare var $:any;

@Component({
    //selector:"addBranch",
    moduleId:module.id,
    templateUrl: 'addCustomerAccount.component.html',
})
export class AddCustomerAccountComponent implements OnInit,OnDestroy,AfterViewInit{
    private customerAcc:CustomerAccount
    form:FormGroup;
    private subscription:any;
    private subscription2:any;
    isLoading:boolean=false;
    failedToLoad = false;
    isEditMode = false;
    notFound = false;
    isBranchesLoading = false;
    branches:Branch[] ;
    filteredCustomers:Customer[];
    customerParameters = {};
    searchTextBox= new FormControl();
    searchLastName = new FormControl();
    searchCustomerId = new FormControl();
    isSearching:boolean = false;
    customerSelected=false;


    constructor(private fb:FormBuilder, 
            private router:Router,
                    private aRoute:ActivatedRoute,
                            private customerAccService:CustomerAccountService,
                            private branchService: BranchService,
                            private toastService:ToastService,
                            private customerService:CustomerService){

        this.customerAcc = new CustomerAccount();

        this.form = this.fb.group({ 
            name:["",Validators.required],
            accountType:["",Validators.required],
            branch:["",Validators.required]
        } )

        this.searchTextBox.valueChanges
                            .debounceTime(400)
                            .distinctUntilChanged()
                            .filter(term => term.length > 2)
                            .do(()=>this.isSearching = true)
                            .switchMap(term => this.customerService.findCustomers(term))
                            .subscribe(
                                    res => {this.filteredCustomers = res.json();
                                        this.isSearching = false;
                                        },
                                    ()=> alert("An error occured"),
                                    //()=> this.isSearching = false
                                    ()=> console.log("Done Searching")
                            );
    }

    gotoDetailsPage(id?){
        if(id)//called when adding a customerAcc
            this.router.navigate(["/CustomerAccountManagement/View",id]);
        else//called after editing a customerAcc sucessfully
            this.router.navigate(["/CustomerAccountManagement/View",this.customerAcc.id]);
        return false;
    }
    
    submit(){

        if (!this.isEditMode) {

            if (!confirm("Are you sure you want to save the customer Account?")) 
                return;

                this.isLoading = true;

            this.subscription2 = this.customerAccService.saveCustomerAccount(this.customerAcc)
            .subscribe(
                (successResponse) => {
                    var id = successResponse.json().id;
                   
                   this.toastService.showSuccessToast("CustomerAccount was saved successfully");
                   this.gotoDetailsPage(id);
                },
                (errorResponse)=>{
                    this.isLoading  = false
                    if (errorResponse.status == 409) {
                        alert(errorResponse.json().message);
                    }
                 
                },
                () => this.isLoading  = false
                 
            );
        }
        else{

            if (!confirm("Are you sure you want to update the customer Account?")) 
                return;

            this.isLoading = true;
            this.subscription2 = this.customerAccService.updateCustomerAccount(this.customerAcc.id,this.customerAcc)
                .subscribe(

                    (successResponse) => {// pass params to details page to show toast

                        this.toastService.showSuccessToast("Customer Account was updated successfully");
                         this.gotoDetailsPage();

                        //this.router.navigate(["/BranchManagement/View",this.branch.id]);//jU as in just updated.
                    },
                    (errorResponse)=>{
                        this.isLoading = false

                        if (errorResponse.status == 409) {
                            alert(errorResponse.json().message);
                        }
                        else if(errorResponse.status == 404){
                            alert("The Customer Account does not exist or has been deleted");
                        }
                    },
                    () => this.isLoading  = false
                );
        }
        
    }
   
    ngOnInit(){

        this.loadBranches();
        // debugger;
        this.aRoute.data.forEach((data:{customerAcc:CustomerAccount}) =>{
            
            if(data.customerAcc){
                this.customerAcc = data.customerAcc;
                this.isEditMode = true;
                this.customerSelected = true;
                this.form.controls["accountType"].disable();
            }
        })
    }
    loadBranches(){
        this.isBranchesLoading = true;
        this.subscription = this.branchService.getAllBranches()
                                .subscribe(

                                    res => {
                                        debugger;
                                        var data = res.json();
                                        this.branches =   data;
                                    },
                                    () => {
                                        alert("Error occured while retrieving list of branches")
                                    },
                                        
                                    () => this.isBranchesLoading = false
                                );
    }
    
    ngOnDestroy(){
        //this.subscription.unsubscribe();
        if(this.subscription2) this.subscription2.unsubscribe();
        //this.subscription2.unsubscribe();
    }

    ngAfterViewInit(){
        $.AdminLTE.layout.activate();
    }

    searchCustomer(){
        var searchTerm = this.searchTextBox.value;
        if(searchTerm=="")
            return;
        this.isSearching = true;
        this.customerService.findCustomers(searchTerm)
                            .subscribe(
                                    res => this.filteredCustomers = res.json(),
                                    ()=> alert("An error occured"),
                                    ()=> this.isSearching = false
                            )
    }
    onCustomerSelected(i:number){
        var selectedCustomer = this.filteredCustomers[i];

        this.customerAcc.customerId = selectedCustomer.id;
        this.customerAcc.customerCustomerId = selectedCustomer.customerID;
        this.customerAcc.customerFirstName = selectedCustomer.firstName;
        this.customerAcc.customerLastName = selectedCustomer.lastName;
        this.customerAcc.customerOtherNames = selectedCustomer.otherNames;

        this.customerAcc.name = 
                `${selectedCustomer.firstName} ${selectedCustomer.lastName}` + 
                    `${selectedCustomer.otherNames? " "+selectedCustomer.otherNames:""}`;
                         
        this.customerSelected = true;

    }
    
 }
