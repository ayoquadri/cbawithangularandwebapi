import {RouterModule,Route } from "@angular/router";
import {NgModule } from "@angular/core";
 import { AuthGuard } from "../../shared"; 
 import {AddCustomerAccountComponent,CustomerAccountDetailsComponent, 
     CustomerAccountListComponent,CustomerAccountResolve} from "./index";

export const routes:Route[] = [
{
    path:"",
    canActivate:[AuthGuard],
    children:[

        {path:'', component:CustomerAccountListComponent},
        {path:'View', component:CustomerAccountListComponent},
        {path:'View/:id', component:CustomerAccountDetailsComponent,
            resolve: {customerAcc: CustomerAccountResolve}},
        {path:'Add', component:AddCustomerAccountComponent},
        {path:'Edit/:id', component:AddCustomerAccountComponent , 
                    resolve:{customerAcc: CustomerAccountResolve}}

    ]
    
}
]
@NgModule({
    imports:[RouterModule.forChild(routes)],
    exports:[RouterModule],
    providers:[CustomerAccountResolve]
})
export class CustomerAccountRoutingModule{}