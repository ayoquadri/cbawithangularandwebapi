import { Component,OnInit,OnDestroy } from '@angular/core';
//import { Control,ControlGroup,FormBuilder,Validators} from '@angular/common';
//import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import { Router, ActivatedRoute} from '@angular/router';
import { Observable } from "rxjs/Observable";

import { BranchService, Branch } from '../shared/index';
declare var $:any;

@Component({
    //selector:"addBranch",
    moduleId :module.id,
    templateUrl: 'branchDetails.component.html',
   // directives:[MyTemplateComponent,ROUTER_DIRECTIVES,SpinnerComponent],
   // providers:[BranchService]
})

export class BranchDetailsComponent  implements OnInit,OnDestroy{
    private branch:Branch;
    private name:string;
    isLoading:boolean = false;
    failedToLoad = false;
    notFound = false;
    private subscription:any;

    constructor(private branchService:BranchService,private route: ActivatedRoute,private router:Router) {
       
       this.branch = new Branch();
    }

    gotoDetailsPage(){

            this.router.navigate(["BranchManagement/View",this.branch.id]);
            return false;
    }

    ngOnInit(){

         this.route.data.forEach((data:{branch:Branch}) =>{
            
            if(data.branch){
                this.branch = data.branch;
            }
         });

    }
    gotoEditPage($event?){
        this.router.navigate(["BranchManagement/Edit",this.branch.id]);
    }    

     gotoReportPage($event?){
          this.router.navigate(["BranchManagement"]);
     }  
        //var id:number;
                                
        //There is no real reason to subscribe to this parmas because it is not be reused it will always be created every time. I could have done this
        // //let id = +this.route.snapshot.params['id'];
        // this.subscription = this.route.params.subscribe(
        //                         params => 
        //                         {
        //                             id = +params['id']; // (+) converts string 'id' to a number
        //                             // var justSaved = params['jS'];
        //                             // var justUpdated = params['jU'];

        //                             // var toastOptions = {
        //                             //         heading:'Branch was saved successfully',
        //                             //         text:"", showHideTransition: 'fade', allowToastClose:true, stack:false,
        //                             //         icon:'success', position : 'top-center',
        //                             //         hideAfter: 5000, loader:false
        //                             //     };


        //                             // if(justSaved){
        //                             //     $.toast(toastOptions)                              
        //                             // }
        //                             // else if(justUpdated){
        //                             //     toastOptions.heading = 'Branch was updated successfully';
        //                             //     $.toast(toastOptions)  
        //                             // }

        //                             this.isLoading = true;

        //                             this.branchService.getBranchById(id)//should return only one branch
        //                                 .subscribe(
        //                                     res => 
        //                                     {
        //                                         this.branch = res;
        //                                     },
        //                                     (errorResponse) =>
        //                                     {
        //                                         console.log("Error Response>>>");
        //                                         console.log(errorResponse)
        //                                         if(errorResponse)
        //                                          {
        //                                              if(errorResponse.status == 404)
        //                                                 this.notFound = true;
        //                                          }   
        //                                         this.failedToLoad = true;
        //                                         this.isLoading = false;
        //                                         alert("Could not retrieve the branch");
        //                                     },
        //                                     () => this.isLoading = false
        //                                 );
        //                         }
        //                     );
       
    ngOnDestroy(){
       // this.subscription.unsubscribe();
    }}



