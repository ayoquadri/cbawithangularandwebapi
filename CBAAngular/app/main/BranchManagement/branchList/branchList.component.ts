import { Component, AfterContentInit, OnInit,ViewChild,OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {Observable} from "rxjs/Rx";
import {Subscription} from "rxjs/Subscription";
//import "rxjs/Observable/of";
// import {PaginatePipe,IPaginationInstance,PaginationControlsCmp,PaginationService} 
//                                 from 'ng2-pagination';

import { BranchService, Branch } from '../shared/index';

//import { SideBarComponent } from '';
@Component({
    //selector:"addBranch",
    moduleId : module.id,
    templateUrl: 'branchList.component.html',
    //directives:[PaginationControlsCmp,MyReportTemplateComponent,MODAL_DIRECTIVES,SpinnerComponent],
    // providers: [PaginationService,BranchService],
    // pipes : [PaginatePipe],
})
export class BranchListComponent implements OnInit,OnDestroy {
    form:FormGroup;
    private _data: Branch[];    
   // private _dataInPage: Branch[];    

    private _page: number = 1;   
    private _pageSize: number = 3;   
    private _total: number= 0;   
    private _totalPages: number;   
    private _isLoading:boolean = false;   
    private isEditMode = false;
    private subscription;
    private searchingObservableSubscription:Subscription=null;
    private searchingObservable:Observable<any>;
    private searchTerm:string="";
    private isSearching = false;
    @ViewChild("selPageSize") selectElement;
    //@ViewChild("modal") modal:ModalComponent;
    
    
    constructor(private fb:FormBuilder,
                 private branchService : BranchService, private router:Router){
        
        this.form = this.fb.group({
            name:["",Validators.required]
        });

        // this.searchingObservable =  
        //                         Observable.empty();
                                //.filter(term => term.length > 0)
                                //.do(()=>console.log("Oya do the do"))
                                //.do(()=>this.isSearching = true)
                                //.switchMap(term => this.branchService.getBranches(1,this._pageSize,this.searchTerm))
                                
                                        

        // this.searchingObservable.subscribe(

        //                             res => {
        //                                 this._total = res.json().count;
        //                                 //this._totalPages = res.json().totalPages;
        //                                 //this._page = page;
        //                                 this._data =   res.json().items;     
        //                             },
        //                             () => {
        //                                 alert("Error occured while retrieving list of branches")
        //                             },
                                        
        //                             () => this._isLoading = false
        //                         );
        
    }
    ngOnInit(){
       this.getPage(1);
    }

    getPage(page:number,searchTerm?:string){
      
        this._isLoading = true;
        this.subscription = this.branchService.getBranches(page,this._pageSize,this.searchTerm)
                                .subscribe(

                                    res => {
                                        this._total = res.json().count;
                                        //this._totalPages = res.json().totalPages;
                                        //this._page = page;
                                        this._data =   res.json().items;     
                                    },
                                    () => {
                                        alert("Error occured while retrieving list of branches")
                                    },
                                        
                                    () => this._isLoading = false
                                );
    }
   
    pageSizeChanged($event){
        if($event.pageSize)
             this._pageSize = $event.pageSize;
        
       
        this.getPage(1);
    }
    //Called by the pagination component when the page changes
    onPageChanged($event){
        if (!$event.page)
            return;
        this._page = $event.page;
        this.getPage($event.page);
    }

    gotoDetailsPage(id:number){
        this.router.navigate(['BranchManagement/View',id]);
    }
    
    showEdit(id:number){
        
        alert("Id>>>"+ id);
        // this.branchServie.getBranchById(id)//should return only one branch
        //                     .subscribe(res => 
        //                     {
        //                         this.branch = res.data
        //                     },
        //                     ()=> alert("Could not retrive the branch"),
        //                     () => this._isLoading = false
        //                     );

       // this.modal.open('lg');
    }
    ngOnDestroy(){
        this.subscription.unsubscribe();
    }

    doSearch(term){
        console.log("dosearch called");
        this.searchingObservableSubscription == null ? "":this.searchingObservableSubscription.unsubscribe();
        this.searchingObservableSubscription = 
                                    Observable.of(term)
                                        .do(()=>this.isSearching = true)
                                        .switchMap(term => this.branchService.getBranches(1,this._pageSize,term))
                                        .subscribe(
                                                    res => {
                                                        this._total = res.json().count;
                                                        //this._totalPages = res.json().totalPages;
                                                        //this._page = page;
                                                        this._data =   res.json().items;     
                                                    },
                                                    () => {
                                                        alert("Error occured while retrieving list of branches")
                                                    },
                                                        
                                                    () => this.isSearching = false
                                                );
       // this.searchTerm = term;
        // this.searchingObservable
        //         .switchMap(term => this.branchService.getBranches(1,this._pageSize,this.searchTerm))
        //         .subscribe(

        //                             res => {
        //                                 this._total = res.json().count;
        //                                 //this._totalPages = res.json().totalPages;
        //                                 //this._page = page;
        //                                 this._data =   res.json().items;     
        //                             },
        //                             () => {
        //                                 alert("Error occured while retrieving list of branches")
        //                             },
                                        
        //                             () => this._isLoading = false
        //                         );

    }

    
 }
