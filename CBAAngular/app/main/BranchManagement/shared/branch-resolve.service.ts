import {Injectable } from '@angular/core';
import { Router, ActivatedRouteSnapshot, Resolve} from '@angular/router';
import {Observable} from "rxjs/Observable";
import "rxjs/add/operator/catch";

import {Branch} from "./branch.model";
import {BranchService} from "./branch.service";

@Injectable()
export class BranchResolve implements Resolve<Branch> {

   constructor(private branchService:BranchService, private router: Router){

   }

   resolve(route:ActivatedRouteSnapshot){

       debugger;
       let id = +route.params["id"];


        return this
                .branchService.getBranchById(id)
                .catch(()=> this.router.navigate(["BranchManagement"]));//should return only one branch
            // .subscribe(
            //     branch =>{
            //         return false;
            //     },
            //     (errorResponse)=>{
            //         this.router.navigate(["BranchManagement"]);
            //         return false;
            //     }
            // );
   }

}