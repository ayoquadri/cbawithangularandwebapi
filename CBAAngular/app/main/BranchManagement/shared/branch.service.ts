import { Injectable } from '@angular/core';
import { Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import "rxjs/add/operator/do";

import { AuthHttp } from '../../../shared/index';
import { Branch } from '../shared/index';


@Injectable()
export class BranchService{
    headers:Headers
    
    url ="http://localhost:58801/api/branches";

    constructor(private _authHttp:AuthHttp){

    }
    // https://angular.io/docs/ts/latest/guide/server-communication.html#!#fetch-data
    getBranches(page:number, pageSize:number,searchTerm?:string){ 
      
      searchTerm = searchTerm? searchTerm:"";
      var searchUrl = this.url + `?searchTerm=${searchTerm}&$skip=${(page-1)*pageSize}&$top=${pageSize}&$inlinecount=allpages`;
      // if(searchTerm)
      //   searchUrl + `& $filter=contains`;

       return this._authHttp
                  .get(searchUrl);
      
   }

   getAllBranches(){
     return this._authHttp.get(this.url+"/getAll");
   }
   
   getBranchById(id:number){
     return this._authHttp
             .get( this.url + "/" + id)
             .map(res=> res.json());
   }
  //  findBranches(searchTerm:string)
  //  {
  //    return this._authHttp.get(`${this.url}/findBranches?search=${searchTerm}`);
  //  }

   saveBranch(branch){

     return this._authHttp
                .post(this.url,JSON.stringify(branch), { headers:this.headers})
                //.map(res =>res..json());
   }
   updateBranch(id:number,branch){
     return this._authHttp
                .put(this.url+ "/"+ id,JSON.stringify(branch), {headers:this.headers})
                //.map(res =>res..json());
   }
  
  private handleError(error:Response){
    //TODO: send the error to remote logging infrastructure
    console.error(error);
    return Observable.throw(error);
  }
   
   
  
}