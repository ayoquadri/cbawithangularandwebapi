import  { NgModule,CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import  { BranchService,BranchResolve } from "./shared/index";
import  { BranchRoutingModule} from "./branch-routing.module";
import { SharedModule } from "../../shared/shared.module";
import { BranchListComponent, BranchDetailsComponent, AddBranchComponent } from "./index";
@NgModule({
    declarations:[BranchListComponent, BranchDetailsComponent, AddBranchComponent],
    imports : [
        SharedModule,
        BranchRoutingModule
    ],
    providers:[BranchService],
    schemas:[CUSTOM_ELEMENTS_SCHEMA]
})
export class BranchModule{}