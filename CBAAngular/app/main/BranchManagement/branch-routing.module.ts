import {RouterModule,Route } from "@angular/router";
import {NgModule } from "@angular/core";

import { AddBranchComponent } from "./addBranch/addBranch.component";
import { BranchListComponent } from "./branchList/branchList.component";
import { BranchDetailsComponent } from "./branchDetails/branchDetails.component";
import {BranchResolve} from "./shared/index";
import { AuthGuard } from "../../shared";


export const routes:Route[] = [

    {
        path:'', 
        canActivate:[AuthGuard],
        children:[
            {path:'', component:BranchListComponent},
            {path:'View', component:BranchListComponent},
            {path:'View/:id', component:BranchDetailsComponent, 
                    resolve: {branch: BranchResolve}},
            {path:'Add', component:AddBranchComponent},
            {path:'Edit/:id', component:AddBranchComponent , 
                        resolve:{branch: BranchResolve}}
        ],
        
    }
    
]

@NgModule({
    imports:[RouterModule.forChild(routes)],
    exports:[RouterModule],
    providers:[BranchResolve]
})
export class BranchRoutingModule{}