import { Component,AfterContentInit,OnInit,Input,OnDestroy,AfterViewInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import { Router, ActivatedRoute} from '@angular/router';

import { Branch, BranchService} from '../shared/index';

declare var $:any;

@Component({
    //selector:"addBranch",
    moduleId:module.id,
    templateUrl: 'addBranch.component.html',
})
export class AddBranchComponent implements OnInit,OnDestroy,AfterViewInit{
    private branch:Branch
    form:FormGroup;
    private subscription:any;
    private subscription2:any;
    isLoading:boolean=false;
    failedToLoad = false;
    isEditMode = false;
    notFound = false;

    constructor(private fb:FormBuilder, 
            private router:Router,
                    private aRoute:ActivatedRoute,
                            private branchService:BranchService){

        this.branch = new Branch();

        this.form = this.fb.group({
            name:["",Validators.required],
            address: ["",Validators.required]
        } )
       
    }

    gotoDetailsPage($event?){
            this.router.navigate(["/BranchManagement/View",this.branch.id]);
            return false;

    }
    
    submit(){

            var toastOptions = {
                                    heading:'Branch was saved successfully',
                                    text:"", showHideTransition: 'fade', allowToastClose:true, stack:false,
                                    icon:'success', position : 'top-center',
                                    hideAfter: 5000, loader:false
                                };


        if (!this.isEditMode) {

            if (!confirm("Are you sure you want to save the branch?")) 
                return;

            this.subscription2 = this.branchService.saveBranch(this.branch)
            .subscribe(
                (successResponse) => {
                    this.branch = successResponse.json();
                   
                   $.toast(toastOptions)                              
                                    
                   // pass params to details page to show toast
                   this.gotoDetailsPage();
                   //this.router.navigate(["/BranchManagement/View",this.branch.id]);//js as in just saved
                },
                (errorResponse)=>{
                    
                    if (errorResponse.status == 409) {
                        alert(errorResponse.json().message);
                    }
                
                } 
            );
        }
        else{

            if (!confirm("Are you sure you want to update the branch?")) 
                return;

            this.isLoading = true;
            this.subscription2 = this.branchService.updateBranch(this.branch.id,this.branch)
                .subscribe(

                    (successResponse) => {// pass params to details page to show toast

                        toastOptions.heading = 'Branch was updated successfully';
                         $.toast(toastOptions)
                         this.gotoDetailsPage();

                        //this.router.navigate(["/BranchManagement/View",this.branch.id]);//jU as in just updated.

                    },
                    (errorResponse)=>{
                        this.isLoading = false

                        if (errorResponse.status == 409) {
                            alert(errorResponse.json().message);
                        }
                        else if(errorResponse.status == 404){
                            alert("The branch does not exist or has been deleted");

                        }
                    },
                    () => this.isLoading  = false
                );
        }
        
    }
   
    ngOnInit(){
        debugger;
        this.aRoute.data.forEach((data:{branch:Branch}) =>{
            
            if(data.branch){
                this.branch = data.branch;
                this.isEditMode = true;
            }

        })
        // var id:number;
        // this.subscription = this.aRoute.params.subscribe(
        //                         params => 
        //                         {
        //                             id = +params['id']; // (+) converts string 'id' to a number
        //                             console.log("ID>>>");
        //                             console.log(id);

        //                             if (id) {
        //                                 this.isEditMode = true;  
        //                                 this.isLoading = true; 

        //                                 this.branchService.getBranchById(id)//should return only one branch
        //                                     .subscribe(
        //                                         res =>{

        //                                             this.branch = res;
        //                                         },
        //                                         (errorResponse)=>{
        //                                             if(errorResponse)
        //                                             {
        //                                                 if(errorResponse.status == 404)
        //                                                     this.notFound = true;
        //                                             }
        //                                             this.isLoading = false;
        //                                             this.failedToLoad = true;
        //                                              alert("Could not retrieve the branch")
        //                                         },
        //                                         () => this.isLoading = false
        //                                     );
        //                             }

        //                         }
        //                     );

    }
    ngOnDestroy(){
        //this.subscription.unsubscribe();
        if(this.subscription2) this.subscription2.unsubscribe();
        //this.subscription2.unsubscribe();
    }

    ngAfterViewInit(){
        $.AdminLTE.layout.activate();
    }
    
 }
