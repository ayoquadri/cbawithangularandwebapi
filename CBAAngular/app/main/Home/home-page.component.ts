import { Component } from '@angular/core';
import { BaseComponent } from '../../shared';

@Component({

    moduleId: module.id,
    
  templateUrl: 'home-page.component.html',
})
export class HomePageComponent extends BaseComponent {
   
    constructor(){
        super();
    }
 }
