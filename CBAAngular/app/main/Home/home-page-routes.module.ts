import {NgModule } from "@angular/core";
import {RouterModule,Route } from "@angular/router";
import { AuthGuard } from "../../shared";
import { HomePageComponent } from "./home-page.component";

 export const routes:Route[] = [

    {path:'', component:HomePageComponent, canActivate:[AuthGuard]},
    //{path:'Login', redi component:AddBranchComponent},
]
@NgModule({
    imports:[RouterModule.forChild(routes)],
    exports:[RouterModule]
})
export class HomeRoutingModule{}
