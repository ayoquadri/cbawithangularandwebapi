import  { NgModule,CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import  { HomeRoutingModule } from "./home-page-routes.module";
import  { HomePageComponent } from "./home-page.component";

import { SharedModule } from "../../shared/shared.module";

@NgModule({
    declarations:[ HomePageComponent],
    imports : [
        SharedModule,
        HomeRoutingModule
    ],
    schemas:[CUSTOM_ELEMENTS_SCHEMA]
})
export class HomePageModule{}