import {RouterModule,Route } from "@angular/router";
import {NgModule } from "@angular/core";
import {AddCustomerComponent,CustomerDetailsComponent,
    CustomerResolve,CustomerListComponent} from "./index";
 import { AuthGuard } from "../../shared";


export const routes:Route[] = [
    {
        path:"",
        canActivate:[AuthGuard],
        children:[

            {path:'', component:CustomerListComponent},
            {path:'View', component:CustomerListComponent},
            {path:'View/:id', component:CustomerDetailsComponent,
                resolve: {customer: CustomerResolve}},
            {path:'Add', component:AddCustomerComponent},
            {path:'Edit/:id', component:AddCustomerComponent , 
                        resolve:{customer: CustomerResolve}}

        ]
        
    }
    
    //{path:'Login', redi component:AddBranchComponent},
]
@NgModule({
    imports:[RouterModule.forChild(routes)],
    exports:[RouterModule],
    providers:[CustomerResolve]
})
export class CustomerRoutingModule{}