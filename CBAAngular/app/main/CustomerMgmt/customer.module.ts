import  { NgModule,CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
 import  { CustomerService, CustomerResolve } from "./shared/index";
 import  { AddCustomerComponent,CustomerDetailsComponent,CustomerListComponent } from "./index";
import  { CustomerRoutingModule } from "./customer-routing.module";
import { SharedModule } from "../../shared/shared.module";

@NgModule({
   declarations:[AddCustomerComponent,CustomerDetailsComponent,CustomerListComponent],
    imports : [
      
        SharedModule,
     
        CustomerRoutingModule
    ],
    providers:[CustomerService],
    
    schemas:[CUSTOM_ELEMENTS_SCHEMA]
})
export class CustomerModule{}