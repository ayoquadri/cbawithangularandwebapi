import { Component,AfterContentInit,OnInit,Input,OnDestroy,AfterViewInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import { Router, ActivatedRoute} from '@angular/router';

import { Customer, CustomerService} from '../shared/index';
import {GlobalValidator,ToastService} from '../../../shared/index';
import  { BranchService,Branch } from "../../BranchManagement/index";

declare var $:any;

@Component({
    //selector:"addBranch",
    moduleId:module.id,
    templateUrl: 'addCustomer.component.html',
})
export class AddCustomerComponent implements OnInit,OnDestroy,AfterViewInit{
    private customer:Customer
    form:FormGroup;
    private subscription:any;
    private subscription2:any;
    isLoading:boolean=false;
    failedToLoad = false;
    isEditMode = false;
    notFound = false;
    isBranchesLoading = false;
    branches:Branch[] ;
    roles = [];

    constructor(private fb:FormBuilder, 
            private router:Router,
                    private aRoute:ActivatedRoute,
                            private customerService:CustomerService,
                            //private branchService: BranchService,
                            private toastService:ToastService){

        this.customer = new Customer();

        this.form = this.fb.group({ 
            firstName:["",Validators.required],
            lastName:["",Validators.required],
            otherName:[""],
            gender:["",Validators.required],
            address:["",Validators.required],
            phoneNumber:["",[Validators.required,GlobalValidator.validatePhoneNumber]],
            email:["",[Validators.required, GlobalValidator.validateEmail]],
        } )
        
    }

    gotoDetailsPage(id?){
        if(id)//called when adding a customer
            this.router.navigate(["/CustomerManagement/View",id]);
        else//called after editing a customer sucessfully
            this.router.navigate(["/CustomerManagement/View",this.customer.id]);
        return false;
    }
    
    submit(){

        if (!this.isEditMode) {

            if (!confirm("Are you sure you want to save the customer?")) 
                return;

                this.isLoading = true;

            this.subscription2 = this.customerService.saveCustomer(this.customer)
            .subscribe(
                (successResponse) => {
                    var id = successResponse.json().id;
                   
                   this.toastService.showSuccessToast("Customer was saved successfully");
                   this.gotoDetailsPage(id);
                },
                (errorResponse)=>{
                    this.isLoading  = false
                    if (errorResponse.status == 409) {
                        alert(errorResponse.json().message);
                    }
                 
                },
                () => this.isLoading  = false
                 
            );
        }
        else{

            if (!confirm("Are you sure you want to update the customer?")) 
                return;

            this.isLoading = true;
            this.subscription2 = this.customerService.updateCustomer(this.customer.id,this.customer)
                .subscribe(

                    (successResponse) => {// pass params to details page to show toast

                        this.toastService.showSuccessToast("Customer was updated successfully");
                         this.gotoDetailsPage();

                        //this.router.navigate(["/BranchManagement/View",this.branch.id]);//jU as in just updated.
                    },
                    (errorResponse)=>{
                        this.isLoading = false

                        if (errorResponse.status == 409) {
                            alert(errorResponse.json().message);
                        }
                        else if(errorResponse.status == 404){
                            alert("The Customer does not exist or has been deleted");
                        }
                    },
                    () => this.isLoading  = false
                );
        }
        
    }
   
    ngOnInit(){

        //this.getBranches(1);
        // debugger;
        this.aRoute.data.forEach((data:{customer:Customer}) =>{
            
            if(data.customer){
                this.customer = data.customer;
                this.isEditMode = true;
            }

        })
    }
    
    ngOnDestroy(){
        //this.subscription.unsubscribe();
        if(this.subscription2) this.subscription2.unsubscribe();
        //this.subscription2.unsubscribe();
    }

    ngAfterViewInit(){
        $.AdminLTE.layout.activate();
    }
    
 }
