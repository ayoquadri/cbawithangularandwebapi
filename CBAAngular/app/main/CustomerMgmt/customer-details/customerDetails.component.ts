import { Component,OnInit,OnDestroy } from '@angular/core';
//import { Control,ControlGroup,FormBuilder,Validators} from '@angular/common';
//import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import { Router, ActivatedRoute} from '@angular/router';
import { Observable } from "rxjs/Observable";

import { CustomerService, Customer } from '../shared/index';
declare var $:any;

@Component({
    //selector:"addCustomer",
    moduleId :module.id,
    templateUrl: 'customerDetails.component.html',
   // directives:[MyTemplateComponent,ROUTER_DIRECTIVES,SpinnerComponent],
   // providers:[CustomerService]
})

export class CustomerDetailsComponent  implements OnInit,OnDestroy{
    private customer:Customer;
    private name:string;
    isLoading:boolean = false;
    failedToLoad = false;
    notFound = false;
    private subscription:any;

    constructor(private customerService:CustomerService,private route: ActivatedRoute,private router:Router) {
       
       this.customer = new Customer();
    }
    ngOnInit(){

         this.route.data.forEach((data:{customer:Customer}) =>{
            
            if(data.customer){
                this.customer = data.customer;
            }
         });

    }
    gotoEditPage($event?){
        this.router.navigate(["CustomerManagement/Edit",this.customer.id]);
    }    

     gotoReportPage($event?){
          this.router.navigate(["CustomerManagement"]);
     }  
        //var id:number;
                                
        //There is no real reason to subscribe to this parmas because it is not be reused it will always be created every time. I could have done this
        // //let id = +this.route.snapshot.params['id'];
        // this.subscription = this.route.params.subscribe(
        //                         params => 
        //                         {
        //                             id = +params['id']; // (+) converts string 'id' to a number
        //                             // var justSaved = params['jS'];
        //                             // var justUpdated = params['jU'];

        //                             // var toastOptions = {
        //                             //         heading:'Customer was saved successfully',
        //                             //         text:"", showHideTransition: 'fade', allowToastClose:true, stack:false,
        //                             //         icon:'success', position : 'top-center',
        //                             //         hideAfter: 5000, loader:false
        //                             //     };


        //                             // if(justSaved){
        //                             //     $.toast(toastOptions)                              
        //                             // }
        //                             // else if(justUpdated){
        //                             //     toastOptions.heading = 'Customer was updated successfully';
        //                             //     $.toast(toastOptions)  
        //                             // }

        //                             this.isLoading = true;

        //                             this.branchService.getCustomerById(id)//should return only one customer
        //                                 .subscribe(
        //                                     res => 
        //                                     {
        //                                         this.customer = res;
        //                                     },
        //                                     (errorResponse) =>
        //                                     {
        //                                         console.log("Error Response>>>");
        //                                         console.log(errorResponse)
        //                                         if(errorResponse)
        //                                          {
        //                                              if(errorResponse.status == 404)
        //                                                 this.notFound = true;
        //                                          }   
        //                                         this.failedToLoad = true;
        //                                         this.isLoading = false;
        //                                         alert("Could not retrieve the customer");
        //                                     },
        //                                     () => this.isLoading = false
        //                                 );
        //                         }
        //                     );
       
    ngOnDestroy(){
       // this.subscription.unsubscribe();
    }
}



