
export class Customer{
   // id :string = "";
    id :number;
    customerID:number;
    gender:string="";
    firstName:string;
    lastName:string;
    otherNames:string;
    password:string;
    confirmPassword:string;
    email:string;
    address:string;
    phoneNumber:string;
}