import {Injectable } from '@angular/core';
import { Router, ActivatedRouteSnapshot, Resolve} from '@angular/router';
import {Observable} from "rxjs/Observable";
import "rxjs/add/operator/catch";

import {Customer} from "./customer.model";
import {CustomerService} from "./customer.service";

@Injectable()
export class CustomerResolve implements Resolve<Customer> {

   constructor(private customerService:CustomerService, private router: Router){

   }

   resolve(route:ActivatedRouteSnapshot){

       debugger;
       //let id = +route.params["id"];
       let id = route.params["id"];


        return this
                .customerService.getCustomerById(id)
                .catch(()=> this.router.navigate(["CustomerManagement"]));//should return only one branch
            // .subscribe(
            //     branch =>{
            //         return false;
            //     },
            //     (errorResponse)=>{
            //         this.router.navigate(["BranchManagement"]);
            //         return false;
            //     }
            // );
   }

}