import { Component,AfterContentInit,OnInit,Input,OnDestroy,AfterViewInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import { Router, ActivatedRoute} from '@angular/router';

import { User, UserService} from '../shared/index';
import {GlobalValidator,ToastService} from '../../../shared/index';
import  { BranchService,Branch } from "../../BranchManagement/index";

declare var $:any;

@Component({
    //selector:"addBranch",
    moduleId:module.id,
    templateUrl: 'addUser.component.html',
})
export class AddUserComponent implements OnInit,OnDestroy,AfterViewInit{
    private user:User
    form:FormGroup;
    private subscription:any;
    private subscription2:any;
    isLoading:boolean=false;
    failedToLoad = false;
    isEditMode = false;
    notFound = false;
    isBranchesLoading = false;
    branches:Branch[] ;
    roles = [];

    constructor(private fb:FormBuilder, 
            private router:Router,
                    private aRoute:ActivatedRoute,
                            private userService:UserService,
                            private branchService: BranchService,
                            private toastService:ToastService){

        this.user = new User();

        this.form = this.fb.group({

            firstName:["",Validators.required],
            lastName:["",Validators.required],
            otherName:[""],
            userName:["",Validators.required],
           // phoneNumber:["",[Validators.required,Validators.pattern("^(?(?=\+234)\+234|0)[7-9]\d[1-9]\d{7}$")]],
            phoneNumber:["",[Validators.required,GlobalValidator.validatePhoneNumber]],
            email:["",[Validators.required, GlobalValidator.validateEmail]],
            // matchPassword:this.fb.group({
            //     password:["", Validators.required],
            //     confirmPassword:["", Validators.required]

            // },{validator:this.doesPasswordMatch}),
            
            branch:["",Validators.required],
            role:["",Validators.required]
            
        } )
        
    }

    doesPasswordMatch(group:FormGroup){
        debugger;
        let password = group.controls["password"].value;
        let confirmPassword = group.controls["confirmPassword"].value;       
        if(password !== confirmPassword)
          return { unmatched : true};
        return null;
    }

    gotoDetailsPage(id?){
        if(id)//called when adding a user
            this.router.navigate(["/UserManagement/View",id]);
        else//called after editing a user sucessfully
            this.router.navigate(["/UserManagement/View",this.user.id]);
        return false;
    }
    
    submit(){

        console.log("Form Errors>>>");
        console.log(this.form);

            var toastOptions = {
                                    heading:'Branch was saved successfully',
                                    text:"", showHideTransition: 'fade', allowToastClose:true, stack:false,
                                    icon:'success', position : 'top-center',
                                    hideAfter: 5000, loader:false
                                };


        if (!this.isEditMode) {

            if (!confirm("Are you sure you want to save the user?")) 
                return;

                this.isLoading = true;

            this.subscription2 = this.userService.saveUser(this.user)
            .subscribe(
                (successResponse) => {
                   // this.user = successResponse.json();
                    var id = successResponse.json().id;
                   
                   this.toastService.showSuccessToast("User was saved successfully");
                   //$.toast(toastOptions)                              
                                    
                   // pass params to details page to show toast
                   this.gotoDetailsPage(id);
                   //this.router.navigate(["/BranchManagement/View",this.branch.id]);//js as in just saved
                },
                (errorResponse)=>{
                    this.isLoading  = false
                    if (errorResponse.status == 409) {
                        alert(errorResponse.json().message);
                    }
                 
                },
                () => this.isLoading  = false
                 
            );
        }
        else{

            if (!confirm("Are you sure you want to update the user?")) 
                return;

            this.isLoading = true;
            this.subscription2 = this.userService.updateUser(this.user.id,this.user)
                .subscribe(

                    (successResponse) => {// pass params to details page to show toast

                        this.toastService.showSuccessToast("User was updated successfully");
                         this.gotoDetailsPage();

                        //this.router.navigate(["/BranchManagement/View",this.branch.id]);//jU as in just updated.
                    },
                    (errorResponse)=>{
                        this.isLoading = false

                        if (errorResponse.status == 409) {
                            alert(errorResponse.json().message);
                        }
                        else if(errorResponse.status == 404){
                            alert("The User does not exist or has been deleted");
                        }
                    },
                    () => this.isLoading  = false
                );
        }
        
    }
   
    ngOnInit(){

        //this.getBranches(1);
        // debugger;
        this.aRoute.data.forEach((data:{user:User}) =>{
            
            if(data.user){
                this.user = data.user;
                this.isEditMode = true;
            }

        })
        this.loadBranchesAndRoles();

    }
    loadBranchesAndRoles(){
        this.isBranchesLoading = true;
        this.subscription = this.userService.getBranchesAndRoles()
                                .subscribe(

                                    res => {
                                        //this._total = res.json().count;
                                        //this._totalPages = res.json().totalPages;
                                        //this._page = page;
                                        var data = res.json();
                                        this.branches =   data.branches;
                                        this.roles = data.roles;     
                                    },
                                    () => {
                                        alert("Error occured while retrieving list of branches")
                                    },
                                        
                                    () => this.isBranchesLoading = false
                                );
    }
    
    ngOnDestroy(){
        //this.subscription.unsubscribe();
        if(this.subscription2) this.subscription2.unsubscribe();
        //this.subscription2.unsubscribe();
    }

    ngAfterViewInit(){
        $.AdminLTE.layout.activate();
    }
    
 }
