import {RouterModule ,Route} from "@angular/router";
import {NgModule,CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
// import { AddUserComponent } from "./user-add/addUser.component";
//  import { UserListComponent } from "./user-list/userList.component";
// import { UserDetailsComponent } from "./user-details/userDetails.component";
import  { AddUserComponent,UserDetailsComponent,UserListComponent } from "./index"
import {UserResolve} from "./shared/index";
import { AuthGuard } from "../../shared";


export const routes:Route[] = [
    {
        path:'',
        canActivate:[AuthGuard],
        children:[
        {path:'', component:UserListComponent},
        {path:'View', component:UserListComponent},
        {path:'View/:id', component:UserDetailsComponent,
                resolve: {user: UserResolve}},
        {path:'Add', component:AddUserComponent},
        {path:'Edit/:id', component:AddUserComponent , 
                    resolve:{user: UserResolve}}
        ]
    }

]

@NgModule({
    imports:[RouterModule.forChild(routes)],
    exports:[RouterModule],
    providers:[UserResolve],
})
export class UserRoutingModule{}