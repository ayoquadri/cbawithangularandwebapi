import  { NgModule,CUSTOM_ELEMENTS_SCHEMA} from "@angular/core";
 import  { AddUserComponent,UserDetailsComponent,UserListComponent } from "./index";
 import  { UserService, UserResolve } from "./shared/index";
 import  { BranchService } from "../BranchManagement/index";
import  { UserRoutingModule } from "./user-routing.module";
import { SharedModule } from "../../shared/shared.module";

@NgModule({
    declarations:[UserListComponent,AddUserComponent, UserDetailsComponent],
    imports : [
      
        SharedModule,
     
        UserRoutingModule
    ],
    providers:[UserService,BranchService],
    
    schemas:[CUSTOM_ELEMENTS_SCHEMA]
})
export class UserModule{}