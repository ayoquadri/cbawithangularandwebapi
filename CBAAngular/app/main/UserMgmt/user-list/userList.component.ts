import { Component, AfterContentInit, OnInit,ViewChild,OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
// import {Observable} from "rxjs/Observable";
import {Observable} from "rxjs/Rx";
import {Subscription} from "rxjs/Subscription";

import { UserService, User } from '../shared/index';

//import { SideBarComponent } from '';
@Component({
    //selector:"adduser",
    moduleId : module.id,
    templateUrl: 'userList.component.html',
    //directives:[PaginationControlsCmp,MyReportTemplateComponent,MODAL_DIRECTIVES,SpinnerComponent],
    // providers: [PaginationService],
    // pipes : [PaginatePipe],
})
export class UserListComponent implements OnInit,OnDestroy {
    form:FormGroup;
    private _data: User[];    
   // private _dataInPage: user[];    

    private _page: number = 1;   
    private _pageSize: number = 3;   
    private _total: number= 0;   
    private _totalPages: number;   
    private _isLoading:boolean = false;   
    private isEditMode = false;
    private subscription;
    private searchingObservableSubscription:Subscription=null;
    private searchingObservable:Observable<any>;
    private isSearching = false;

    
    @ViewChild("selPageSize") selectElement;
    //@ViewChild("modal") modal:ModalComponent;
    
    
    constructor(private fb:FormBuilder,
                 private userService : UserService, private router:Router){
        
        this.form = this.fb.group({
            name:["",Validators.required]
        });
        
    }
    ngOnInit(){
       this.getPage(1);
    }

    getPage(page:number){
      
        this._isLoading = true;
        this.subscription = this.userService.getUsers(page,this._pageSize)
                                .subscribe(

                                    res => {
                                        this._total = res.json().count;
                                        //this._totalPages = res.json().totalPages;
                                        //this._page = page;
                                        this._data =   res.json().items;     
                                    },
                                    () => {
                                        alert("Error occured while retrieving list of users")
                                    },
                                        
                                    () => this._isLoading = false
                                );
    }
   
    pageSizeChanged($event){
        if($event.pageSize)
             this._pageSize = $event.pageSize;
        
       
        this.getPage(1);
    }
    //Called by the pagination component when the page changes
    onPageChanged($event){
        if (!$event.page)
            return;
        this._page = $event.page;
        this.getPage($event.page);
    }

    gotoDetailsPage(id:number){
        this.router.navigate(['UserManagement/View',id]);
    }
    
    showEdit(id:number){
        
        alert("Id>>>"+ id);
    }
    ngOnDestroy(){
        this.subscription.unsubscribe();
    }
    doSearch(term){
        console.log("dosearch called");
        this.searchingObservableSubscription == null ? "":this.searchingObservableSubscription.unsubscribe();
        this.searchingObservableSubscription = 
                                    Observable.of(term)
                                        .do(()=>this.isSearching = true)
                                        .switchMap(term => this.userService.getUsers
                                                                    (1,this._pageSize,term))
                                        .subscribe(
                                                    res => {
                                                        this._total = res.json().count;
                                                        //this._totalPages = res.json().totalPages;
                                                        //this._page = page;
                                                        this._data =   res.json().items;     
                                                    },
                                                    () => {
                                                        alert("Error occured while retrieving list of Users")
                                                    },
                                                        
                                                    () => this.isSearching = false
                                                );
    
 }
}
