import { Component,OnInit,OnDestroy } from '@angular/core';
//import { Control,ControlGroup,FormBuilder,Validators} from '@angular/common';
//import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import { Router, ActivatedRoute} from '@angular/router';
import { Observable } from "rxjs/Observable";

import { UserService, User } from '../shared/index';
declare var $:any;

@Component({
    //selector:"addUser",
    moduleId :module.id,
    templateUrl: 'userDetails.component.html',
   // directives:[MyTemplateComponent,ROUTER_DIRECTIVES,SpinnerComponent],
   // providers:[UserService]
})

export class UserDetailsComponent  implements OnInit,OnDestroy{
    private user:User;
    private name:string;
    isLoading:boolean = false;
    failedToLoad = false;
    notFound = false;
    private subscription:any;

    constructor(private userService:UserService,private route: ActivatedRoute,private router:Router) {
       
       this.user = new User();
    }

    gotoDetailsPage(){

            this.router.navigate(["UserManagement/View",this.user.id]);
            return false;
    }

    ngOnInit(){

         this.route.data.forEach((data:{user:User}) =>{
            
            if(data.user){
                this.user = data.user;
            }
         });

    }
    gotoEditPage($event?){
        this.router.navigate(["UserManagement/Edit",this.user.id]);
    }    

     gotoReportPage($event?){
          this.router.navigate(["UserManagement"]);
     }  
        //var id:number;
                                
        //There is no real reason to subscribe to this parmas because it is not be reused it will always be created every time. I could have done this
        // //let id = +this.route.snapshot.params['id'];
        // this.subscription = this.route.params.subscribe(
        //                         params => 
        //                         {
        //                             id = +params['id']; // (+) converts string 'id' to a number
        //                             // var justSaved = params['jS'];
        //                             // var justUpdated = params['jU'];

        //                             // var toastOptions = {
        //                             //         heading:'User was saved successfully',
        //                             //         text:"", showHideTransition: 'fade', allowToastClose:true, stack:false,
        //                             //         icon:'success', position : 'top-center',
        //                             //         hideAfter: 5000, loader:false
        //                             //     };


        //                             // if(justSaved){
        //                             //     $.toast(toastOptions)                              
        //                             // }
        //                             // else if(justUpdated){
        //                             //     toastOptions.heading = 'User was updated successfully';
        //                             //     $.toast(toastOptions)  
        //                             // }

        //                             this.isLoading = true;

        //                             this.branchService.getUserById(id)//should return only one user
        //                                 .subscribe(
        //                                     res => 
        //                                     {
        //                                         this.user = res;
        //                                     },
        //                                     (errorResponse) =>
        //                                     {
        //                                         console.log("Error Response>>>");
        //                                         console.log(errorResponse)
        //                                         if(errorResponse)
        //                                          {
        //                                              if(errorResponse.status == 404)
        //                                                 this.notFound = true;
        //                                          }   
        //                                         this.failedToLoad = true;
        //                                         this.isLoading = false;
        //                                         alert("Could not retrieve the user");
        //                                     },
        //                                     () => this.isLoading = false
        //                                 );
        //                         }
        //                     );
       
    ngOnDestroy(){
       // this.subscription.unsubscribe();
    }}



