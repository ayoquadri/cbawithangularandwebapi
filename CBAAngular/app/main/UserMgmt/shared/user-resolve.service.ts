import {Injectable } from '@angular/core';
import { Router, ActivatedRouteSnapshot, Resolve} from '@angular/router';
import {Observable} from "rxjs/Observable";
import "rxjs/add/operator/catch";

import {User} from "./user.model";
import {UserService} from "./user.service";

@Injectable()
export class UserResolve implements Resolve<User> {

   constructor(private userService:UserService, private router: Router){

   }

   resolve(route:ActivatedRouteSnapshot){

       debugger;
       //let id = +route.params["id"];
       let id = route.params["id"];


        return this
                .userService.getUserById(id)
                .catch(()=> this.router.navigate(["UserManagement"]));//should return only one branch
            // .subscribe(
            //     branch =>{
            //         return false;
            //     },
            //     (errorResponse)=>{
            //         this.router.navigate(["BranchManagement"]);
            //         return false;
            //     }
            // );
   }

}