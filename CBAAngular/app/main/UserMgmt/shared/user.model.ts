
export class User{
    id :string = "";
    firstName:string;
    lastName:string;
    otherNames:string;
    password:string;
    confirmPassword:string;
    email:string;
    username:string;
    address:string;
    branchName:string;
    branchId:string="";
    phoneNumber:string;
    role: string="";
}