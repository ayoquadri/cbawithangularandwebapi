import { Injectable } from '@angular/core';
import { Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import "rxjs/add/operator/do";

import { AuthHttp } from '../../../shared/index';
//import { Branch } from '../shared/index';


@Injectable()
export class UserService{
    headers:Headers
    
    url ="http://localhost:58801/api/users";

    constructor(private _authHttp:AuthHttp){

    }

    getBranchesAndRoles(){
       return this._authHttp
                      .get( this.url + "/" + "getBranchesAndRoles");
    }

    // https://angular.io/docs/ts/latest/guide/server-communication.html#!#fetch-data
    getUsers(page:number, pageSize:number,searchTerm?:string){ 
    
              searchTerm = searchTerm? searchTerm:"";
      var searchUrl = this.url + `?searchTerm=${searchTerm}&$skip=${(page-1)*pageSize}&$top=${pageSize}&$inlinecount=allpages`;
    
       return this._authHttp
                  .get(searchUrl);
   }
   
   getUserById(id:string){

     return this._authHttp
             .get( this.url + "/" + id)
             .map(res=> res.json());
   }

   saveUser(user){

     return this._authHttp
                .post("http://localhost:58801/api/Account/Register"
                    ,JSON.stringify(user), { headers:this.headers});
   }
   updateUser(id:string,user){
     return this._authHttp
                .put(this.url+ "/"+ id,JSON.stringify(user), {headers:this.headers})
   }
  
  private handleError(error:Response){
    //TODO: send the error to remote logging infrastructure
    console.error(error);
    return Observable.throw(error);
  }
   
   
  
}