import { Component,AfterViewInit ,Injectable} from '@angular/core';
import { CanActivate,
                ActivatedRouteSnapshot,RouterStateSnapshot,Router } from '@angular/router';
import { AuthService } from '../../shared';
//import { AddBranchComponent } from './branchManagement/addBranch.component';

@Injectable()
export class LoginGuard implements CanActivate {


    constructor(private _authService: AuthService,private _router:Router){

    }

    canActivate(route:ActivatedRouteSnapshot, state:RouterStateSnapshot):boolean{

        if(this._authService.isLoggedIn() == true) {
            this._router.navigate(["Home"]);
        }

        return true;
    }
}