import {NgModule } from "@angular/core";
import {RouterModule,Route } from "@angular/router";

import { LoginComponent } from "./login.component";
import { LoginGuard } from "./login.guard";

 export const routes:Route[] = [

    {path:'', component:LoginComponent, canActivate:[LoginGuard]},
    //{path:'Login', redi component:AddBranchComponent},
]
@NgModule({
    imports:[RouterModule.forChild(routes)],
    exports:[RouterModule],
    providers :[ LoginGuard ]
})
export class LoginRoutingModule{}
