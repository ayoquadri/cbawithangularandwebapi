import  { NgModule } from "@angular/core";
//import  { CommonModule } from "@angular/common";
//import  { ReactiveFormsModule,FormsModule } from "@angular/forms";
 
import  { LoginRoutingModule } from "./login-routing.module";
import  { LoginComponent } from "./login.component";
import  { LoginGuard } from "./login.guard";

import { SharedModule } from "../../shared/shared.module";

@NgModule({
    declarations:[ LoginComponent ],
    imports : [
      
        SharedModule,
     
        LoginRoutingModule
    ]
})
export class LoginModule{}