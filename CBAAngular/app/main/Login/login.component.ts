import { Component,OnInit,OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Http, Headers, URLSearchParams, Response} from "@angular/http";
import { Router } from '@angular/router';

import { AuthService,ITokenInfo, TOKEN_NAME} from '../../shared/';



declare var $:any;
@Component({
    moduleId : module.id,
    //selector:"addBranch",
    templateUrl: 'login.component.html',
    styles:[`
        body{

            background:#d2d6de !important;
        }
    `]

})
export class LoginComponent  implements OnInit, OnDestroy {

    form:FormGroup;
    private _username:string;
    private _password:string;

    private _isLoading = false;

    constructor(private _fb: FormBuilder,private _authService: AuthService, private _router:Router, private _http:Http){
      
    }

    ngOnInit(){

        this.form = this._fb.group({
            username:["",Validators.required],
            password:["",Validators.required]
        });

    }

    ngOnDestroy(){
        
    }

    submit(){

        this._isLoading = true;

         this._authService.login(this._username,this._password)
            .subscribe (

                (response)=>{
                    let tokenInfo : ITokenInfo = response.json();

                    localStorage.setItem(TOKEN_NAME,tokenInfo.access_token);

                    let redirectUrl = this._authService.redirectUrl ? this._authService.redirectUrl : "Home";
                    
                    this._router.navigate([redirectUrl]);
                },

                (error)=>{

                    this._isLoading = false;

                    let errorResponse:LoginResponse = error.json();

                    if(errorResponse.error_description != undefined)
                        alert(errorResponse.error_description);
                    else
                        alert("Error Occured: Please Check your network settings");

                    // }
                    // else{ //most likely due to internet connection
                    //     alert("Error Occured: Please Check your network settings");
                    // }
                },
                () => { this._isLoading = false}

            );
         

       

    }
}

export interface LoginResponse{
    error:string;
    error_description:string;
}