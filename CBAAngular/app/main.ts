// import { bootstrap }    from '@angular/platform-browser-dynamic';
// import { HTTP_PROVIDERS }    from '@angular/http';
// import { FORM_PROVIDERS }    from '@angular/common';
// import { AppComponent } from './app.component';
// import { APP_ROUTER_PROVIDERS } from './shared/';
// import { AuthHttp} from './shared/services';
// import { provide } from '@angular/core';

// bootstrap(AppComponent,[APP_ROUTER_PROVIDERS, HTTP_PROVIDERS,FORM_PROVIDERS,AuthHttp]);

import { platformBrowser } from '@angular/platform-browser';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

// The app module factory produced by the static offline compiler

//import { AppModuleNgFactory } from "./app.module.ngfactory";
import { AppModule } from "./app.module";

// Launch with the app module factory.
//platformBrowser().bootstrapModuleFactory(AppModuleNgFactory)
platformBrowserDynamic().bootstrapModule(AppModule)
            // bootstrap(AppComponent,[APP_ROUTER_PROVIDERS, HTTP_PROVIDERS,FORM_PROVIDERS,
            // provide(AuthHttp,{useClass:AuthHttp}),provide(AuthService,{useClass:AuthService})]);
