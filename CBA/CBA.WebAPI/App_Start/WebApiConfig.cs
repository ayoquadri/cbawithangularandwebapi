﻿using System.Web.Http;
using System.Web.Http.Cors;
using Microsoft.Owin.Security.OAuth;
using Newtonsoft.Json.Serialization;

namespace CBA.WebAPI
{
    public static class WebApiConfig
    {
        //private static readonly object UnityBootstrapper;

        public static void Register(HttpConfiguration config)
        {
            //ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
            //builder.EntitySet<Branch>("Branches");
            //config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());

            // Web API configuration and services
            // Configure Web API to use only bearer token authentication.
            config.SuppressDefaultHostAuthentication();
            config.Filters.Add(new HostAuthenticationFilter(OAuthDefaults.AuthenticationType));
            config.Filters.Add(new System.Web.Http.AuthorizeAttribute());

            var corAttribute = new EnableCorsAttribute("*", "*", "*");
            config.EnableCors(corAttribute);

            config.Formatters.JsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();

            config.DependencyResolver = new UnityResolver(UnityBootstrapper.BuildUnityContainer());

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",                       
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
