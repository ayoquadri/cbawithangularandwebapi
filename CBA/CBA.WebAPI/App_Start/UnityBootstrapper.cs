﻿using System;
using System.Collections.Generic;
using System.Web.Http.Dependencies;
using CBA.Core.Persitence.Repositories;
using CBA.Persistence.Repositories;
using CBA.WebAPI.Controllers;
using Microsoft.Practices.Unity;

//using System.Web.Mvc;

namespace CBA.WebAPI
{
    public class UnityResolver : IDependencyResolver
    {
        protected IUnityContainer container;

        public UnityResolver(IUnityContainer container)
        {
            if (container == null)
            {
                throw new ArgumentNullException("container");
            }
            this.container = container;
        }

        public object GetService(Type serviceType)
        {
            try
            {
                return container.Resolve(serviceType);
            }
            catch (ResolutionFailedException)
            {
                return null;
            }
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            try
            {
                return container.ResolveAll(serviceType);
            }
            catch (ResolutionFailedException)
            {
                return new List<object>();
            }
        }

        public IDependencyScope BeginScope()
        {
            var child = container.CreateChildContainer();
            return new UnityResolver(child);
        }

        public void Dispose()
        {
            container.Dispose();
        }
    }
    public class UnityBootstrapper
    {
        //public static IUnityContainer Initialise()
        //{
        //    var container = BuildUnityContainer();
        //    DependencyResolver.SetResolver(new UnityDependencyResolver() );
        //    return container;
        //}
        public static IUnityContainer BuildUnityContainer()
        {
            var container = new UnityContainer();

            //container.RegisterType<IBranchDAL, BranchDAL>();

            container.RegisterType<IBranchRepository, BranchRespository>();
            container.RegisterType<IUserRepository, UserRespository>();
            container.RegisterType<ICustomerRepository, CustomerRespository>();
            container.RegisterType<ICustomerAccountRepository, CustomerAccountRepository>();
            container.RegisterType<IRoleRepository,RoleRespository>();
            container.RegisterType<AccountController, AccountController>();
            //container.RegisterType<ILogger, FakeLogger>();
            //MvcUnityContainer.Container = container;
            return container;
        }
    }


}