﻿using AutoMapper;
using CBA.Core.DTOs;
using CBA.Core.Entities;
using CBA.WebAPI.Models;

namespace CBA.WebAPI
{
    public static class AutoMapperConfig
    {
        //private static readonly object UnityBootstrapper;

        public static void RegisterMappings()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<Branch, BranchDto>().ReverseMap();
                cfg.CreateMap<CustomerAccount, CustomerAccountDto>().
                                ForMember(t => t.AccountType, opt => opt.MapFrom(m => (int)m.AccountType)).ReverseMap();
                cfg.CreateMap<ApplicationUser, UserDto>().ReverseMap();
                cfg.CreateMap<Customer, CustomerDto>().
                        ForMember(t => t.Gender, opt => opt.MapFrom(m => (int)m.Gender)).ReverseMap();
                cfg.CreateMap<RegisterBindingModel, ApplicationUser>().ReverseMap();
            }
        );

         }
    }
}
