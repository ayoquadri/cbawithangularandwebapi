﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.OData;
using System.Web.Http.OData.Extensions;
using System.Web.Http.OData.Query;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using CBA.Core.DTOs;
using CBA.Core.Entities;
using CBA.Core.Persitence.Repositories;

namespace CBA.WebAPI.Controllers
{
    [RoutePrefix("api/Branches")]
    public class BranchesController : ApiController
    {
        private readonly IBranchRepository _branchRepository;

        public BranchesController(IBranchRepository branchRepository)
        {
            _branchRepository = branchRepository;
        }

        //[Route("query")]
        //[HttpGet]
        public IHttpActionResult Get(string searchTerm, ODataQueryOptions<BranchDto> queryOptions)
        {
            try
            {
                var branches = (IQueryable<BranchDto>)queryOptions
                                                    .ApplyTo(_branchRepository.GetQueryableBranches(searchTerm));

                return Ok(new PageResult<BranchDto>(branches, queryOptions.Request.ODataProperties().NextLink,
                                queryOptions.Request.ODataProperties().TotalCount));
            } 
            catch (Exception ex)
            {
                return BadRequest("Error retrieving Branch list - " + ex.Message);
            }
        }
        [ResponseType(typeof(List<BranchDto>))]
        [Route("getAll")]
        public IHttpActionResult GetAll()
        {
            var branches = _branchRepository.GetAll().ProjectTo<BranchDto>().ToList();
            return Ok(branches);
        }


        //// GET: api/Branches
        //[Route("{pageSize:int}/{pageNumber:int}")]
        //public IHttpActionResult GetBranches(int pageSize,int pageNumber)
        //{
        //    int totalCount  = _branchRepository.Count(); 
        //    var totalPages = Math.Ceiling((double)totalCount/pageSize);
        //    var branchQuery = _branchRepository.GetAll();


        //    var branches = branchQuery.OrderBy(b=> b.Id).Skip(pageSize * (pageNumber - 1)).Take(pageSize).ToList();

        //    var result = new PagedResponse<Branch>
        //    {
        //        Data = branches,
        //        TotalPages = totalPages,
        //        Total = totalCount
        //    };
        //    return Ok(result);
        //}

        // GET: api/Branches/5
        [ResponseType(typeof(Branch))]
        public async Task<IHttpActionResult> GetBranch(int id)
        {
            Branch branch = await _branchRepository.Get(id);

            if (branch == null)
            {
                return NotFound();
            }

           var branchDto = Mapper.Map<BranchDto>(branch);

            //var branchDto = new BranchDto
            //{
            //    Id = branch.Id,
            //    Address = branch.Address,
            //    IsBranchOpen = branch.IsBranchOpen,
            //    Name = branch.Name
            //};

            return Ok(branchDto);
        }

        // PUT: api/Branches/5
        [ResponseType(typeof(BranchDto))]
        public async Task<IHttpActionResult> PutBranch(int id, BranchDto branchDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != branchDto.Id)
            {
                return BadRequest();
            }
            var branch = Mapper.Map<Branch>(branchDto);
            try
            {
               // await _branchRepository.Update(branch);
                var updatedBranch = await _branchRepository.DetachAndUpdate(id,branch);
                if (updatedBranch == null)
                    return NotFound();
            }
            catch (DbUpdateConcurrencyException)
            {
                bool branchExists = _branchRepository.Count( b=>b.Id == branch.Id) > 0;
                
                if (!branchExists)
                {
                    return NotFound();
                }
                else
                {
                    var responseMsg = Request.CreateErrorResponse(HttpStatusCode.Conflict, 
                                            "The content of the branch has changed since"+
                                                      " you last accessed it. Do you want to reload the page?");
                    return ResponseMessage(responseMsg);
                }
            }
            catch (DbUpdateException)
            {
                if (_branchRepository.GetBranchByName(branch.Name) != null)
                {
                    var responseMsg = Request.CreateErrorResponse(HttpStatusCode.Conflict, 
                                "A branch with the name '" + branch.Name + "' already exists.");

                    return ResponseMessage(responseMsg);
                    
                }
                return InternalServerError();
                //throw;
            }

            //return StatusCode(HttpStatusCode.NoContent);
            return Ok(branchDto);
        }

        // POST: api/Branches
        [ResponseType(typeof(BranchDto))]
        public async Task<IHttpActionResult> PostBranch(BranchDto branchDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var branch = Mapper.Map<Branch>(branchDto);
            try
            {
                await _branchRepository.Save(branch);
            }
            catch (DbUpdateException)
            {
                if (_branchRepository.GetBranchByName(branch.Name)!= null)
                {
                    var responseMsg =  Request.CreateErrorResponse(HttpStatusCode.Conflict, "A branch with the name '" 
                                        + branch.Name + "' already exists.");
                    return ResponseMessage(responseMsg);
                }

                throw;
            }
            branchDto = Mapper.Map<BranchDto>(branch);
            return CreatedAtRoute("DefaultApi", new { id = branch.Id }, branchDto);
        }



        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _branchRepository.Dispose();
            }
            base.Dispose(disposing);
        }

        //private bool BranchExists(int id)
        //{
        //    return db.Branches.Count(e => e.Id == id) > 0;
        //}


    }
}