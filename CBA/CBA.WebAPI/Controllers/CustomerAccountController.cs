﻿using System;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.OData;
using System.Web.Http.OData.Extensions;
using System.Web.Http.OData.Query;
using AutoMapper;
using CBA.Core.DTOs;
using CBA.Core.Entities;
using CBA.Core.Persitence.Repositories;

namespace CBA.WebAPI.Controllers
{
    [RoutePrefix("api/customerAccounts")]
    public class CustomerAccountsController : ApiController
    {
        
        private readonly ICustomerAccountRepository _customerAccountRepository;

        public CustomerAccountsController(ICustomerAccountRepository customerAccountRepository)
        {
            _customerAccountRepository = customerAccountRepository;
           
        }

        //[Route("query")]
        //[HttpGet]
        public IHttpActionResult Get(string searchTerm,ODataQueryOptions<CustomerAccountDto> queryOptions)
        {
            try
            {
                var customerAccounts = (IQueryable<CustomerAccountDto>) queryOptions.ApplyTo(_customerAccountRepository.GetQueryableCustomerAccounts( searchTerm));

                return Ok(new PageResult<CustomerAccountDto>(customerAccounts, queryOptions.Request.ODataProperties().NextLink,
                    queryOptions.Request.ODataProperties().TotalCount));
            }
            catch (Exception ex)
            {
                return BadRequest("Error retrieving CustomerAccounts list - " + ex.Message);
            }
        }
        
    // GET: api/Branches/5
        [ResponseType(typeof(CustomerAccountDto))]
        public async Task<IHttpActionResult> GetCustomerAccount(int id)
        {
            CustomerAccount customerAccount = await _customerAccountRepository.GetCustomerAccount(id);
            
            if (customerAccount == null)
            {
                return NotFound();
            }
            var customerAccountDto = Mapper.Map<CustomerAccountDto>(customerAccount);

            return Ok(customerAccountDto);
        }

        // PUT: api/Branches/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutCustomerAccount(int id, CustomerAccountDto customerAccountDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != customerAccountDto.Id)
            {
                return BadRequest();
            }
            
            var customerAccount = Mapper.Map<CustomerAccount>(customerAccountDto);

            //ApplicationCustomerAccount updatedCustomerAccount;
            try
            {
                //await _customerAccountRepository.Update(customerAccount);

                CustomerAccount updatedCustomerAccount = await _customerAccountRepository.UpdateCustomerAccount(id,customerAccount);
                if (updatedCustomerAccount == null)
                   return NotFound();
            }
            catch (DbUpdateConcurrencyException)
            {
                bool customerAccountExists = _customerAccountRepository.Count( b=>b.Id == customerAccount.Id) > 0;
                
                if (!customerAccountExists)
                {
                    return NotFound();
                }
                else
                {
                    //var responseMsg = Request.CreateErrorResponse(HttpStatusCode.Conflict, 
                    //                        "The content of the customerAccount has changed since"+
                    //                                  " you last accessed it. Do you want to reload the page?");
                    return Content<string>(HttpStatusCode.Conflict,
                                            "The content of the customerAccount has changed since" +
                                                      " you last accessed it. Do you want to reload the page?");
                   // return ResponseMessage(responseMsg);
                }
            }
            catch (DbUpdateException)
            {
                return InternalServerError();
                //throw;
            }

            //return StatusCode(HttpStatusCode.NoContent);
            return Ok(customerAccountDto);
        }

        // POST: api/Branches
        [ResponseType(typeof(CustomerAccount))]
        public async Task<IHttpActionResult> PostCustomerAccount(CustomerAccountDto customerAccountDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var customerAccount = Mapper.Map<CustomerAccount>(customerAccountDto);
            try
            {
                await _customerAccountRepository.AddNewCustomerAccount(customerAccount);
            }
            catch (DbUpdateException)
            {
                return InternalServerError();
                throw;
            }

            return CreatedAtRoute("DefaultApi", new { id = customerAccount.Id }, customerAccount);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _customerAccountRepository.Dispose();
            }
            base.Dispose(disposing);
        }

        //private bool BranchExists(int id)
        //{
        //    return db.Branches.Count(e => e.Id == id) > 0;
        //}


    }
}