﻿using System;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.OData;
using System.Web.Http.OData.Extensions;
using System.Web.Http.OData.Query;
using AutoMapper;
using CBA.Core.DTOs;
using CBA.Core.Entities;
using CBA.Core.Persitence.Repositories;

namespace CBA.WebAPI.Controllers
{
    [RoutePrefix("api/users")]
    public class UsersController : ApiController
    {
        private readonly IBranchRepository _branchRepository;
        private readonly IUserRepository _userRepository;
        private readonly IRoleRepository _roleRepository;

        public UsersController(IUserRepository userRepository, 
                                IBranchRepository branchRepository, 
                                        IRoleRepository roleRepository)
        {
            _userRepository = userRepository;
            _branchRepository = branchRepository;
            _roleRepository = roleRepository;
        }

        //[Route("query")]
        //[HttpGet]
        public IHttpActionResult Get(string searchTerm,ODataQueryOptions<UserDto> queryOptions)
        {
            try
            {
                var users = (IQueryable<UserDto>) queryOptions.ApplyTo(_userRepository.GetQueryableUsers(searchTerm));

                return Ok(new PageResult<UserDto>(users, queryOptions.Request.ODataProperties().NextLink,
                    queryOptions.Request.ODataProperties().TotalCount));
            }
            catch (Exception ex)
            {
                return BadRequest("Error retrieving Users list - " + ex.Message);
            }
        }

        [Route("getBranchesAndRoles")]
        public IHttpActionResult GetBranchesAndRoles()
        {
            var branches = _branchRepository.GetQueryableBranches().ToList();
            var roles = _roleRepository.GetAllRoles().Select(r => new
            {
                Id = r.Id,
                Name = r.Name
            });

            var branchesAndRoles  = new
            {
                branches = branches,
                roles  = roles
            };
            return Ok(branchesAndRoles);
        }



    // GET: api/Branches/5
        [ResponseType(typeof(UserDto))]
        public async Task<IHttpActionResult> GetUser(string id)
        {
            ApplicationUser user = await _userRepository.GetUser(id);
            
            if (user == null)
            {
                return NotFound();
            }
            var userDto = Mapper.Map<UserDto>(user);

            return Ok(userDto);
        }

        // PUT: api/Branches/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutUser(string id, UserDto userDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != userDto.Id)
            {
                return BadRequest();
            }
            
            var user = Mapper.Map<ApplicationUser>(userDto);

            //ApplicationUser updatedUser;
            try
            {
                //await _userRepository.Update(user);

                ApplicationUser updatedUser = await _userRepository.DetachAndUpdate(id,user);
                if (updatedUser == null)
                   return NotFound();
            }
            catch (DbUpdateConcurrencyException)
            {
                bool userExists = _userRepository.Count( b=>b.Id == user.Id) > 0;
                
                if (!userExists)
                {
                    return NotFound();
                }
                else
                {
                    //var responseMsg = Request.CreateErrorResponse(HttpStatusCode.Conflict, 
                    //                        "The content of the user has changed since"+
                    //                                  " you last accessed it. Do you want to reload the page?");
                    return Content<string>(HttpStatusCode.Conflict,
                                            "The content of the user has changed since" +
                                                      " you last accessed it. Do you want to reload the page?");
                   // return ResponseMessage(responseMsg);
                }
            }
            catch (DbUpdateException)
            {
                return InternalServerError();
                //throw;
            }

            //return StatusCode(HttpStatusCode.NoContent);
            return Ok(userDto);
        }

        // POST: api/Branches
        [ResponseType(typeof(Branch))]
        public async Task<IHttpActionResult> PostBranch(ApplicationUser user)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                await _userRepository.Save(user);
            }
            catch (DbUpdateException)
            {
                return InternalServerError();
                throw;
            }

            return CreatedAtRoute("DefaultApi", new { id = user.Id }, user);
        }



        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _userRepository.Dispose();
            }
            base.Dispose(disposing);
        }

        //private bool BranchExists(int id)
        //{
        //    return db.Branches.Count(e => e.Id == id) > 0;
        //}


    }
}