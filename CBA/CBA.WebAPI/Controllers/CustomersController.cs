﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.OData;
using System.Web.Http.OData.Extensions;
using System.Web.Http.OData.Query;
using AutoMapper;
using CBA.Core.DTOs;
using CBA.Core.Entities;
using CBA.Core.Persitence.Repositories;

namespace CBA.WebAPI.Controllers
{
    [RoutePrefix("api/customers")]
    public class CustomersController : ApiController
    {
        
        private readonly ICustomerRepository _customerRepository;

        public CustomersController(ICustomerRepository customerRepository)
        {
            _customerRepository = customerRepository;
           
        }

        //[Route("query")]
        //[HttpGet]
        public IHttpActionResult Get(string searchTerm, ODataQueryOptions<CustomerDto> queryOptions)
        {
            try
            {
                var customers = (IQueryable<CustomerDto>)queryOptions.ApplyTo(_customerRepository.GetQueryableCustomers(searchTerm));

                return Ok(new PageResult<CustomerDto>(customers, queryOptions.Request.ODataProperties().NextLink,
                    queryOptions.Request.ODataProperties().TotalCount));
            }
            catch (Exception ex)
            {
                return BadRequest("Error retrieving Customers list - " + ex.Message);
            }
        }
        
    // GET: api/Branches/5
        [ResponseType(typeof(CustomerDto))]
        public async Task<IHttpActionResult> GetCustomer(int id)
        {
            Customer customer = await _customerRepository.Get(id);
            
            if (customer == null)
            {
                return NotFound();
            }
            var customerDto = Mapper.Map<CustomerDto>(customer);

            return Ok(customerDto);
        }
        [ResponseType(typeof(CustomerDto))]
        [Route("findCustomers")]
        [HttpGet]
        public async Task<IHttpActionResult> FindCustomers(string search)
        {
            var foundCustomers = new List<CustomerDto>();

            if (!string.IsNullOrEmpty(search))
            {
                foundCustomers = await _customerRepository.FindCustomers(search).ToListAsync();
            }
            
            return Ok(foundCustomers);
        }
        // PUT: api/Branches/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutCustomer(int id, CustomerDto customerDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != customerDto.Id)
            {
                return BadRequest();
            }
            
            var customer = Mapper.Map<Customer>(customerDto);

            //ApplicationCustomer updatedCustomer;
            try
            {
                //await _customerRepository.Update(customer);

                Customer updatedCustomer = await _customerRepository.UpdateCustomer(id,customer);
                if (updatedCustomer == null)
                   return NotFound();
            }
            catch (DbUpdateConcurrencyException)
            {
                bool customerExists = _customerRepository.Count( b=>b.Id == customer.Id) > 0;
                
                if (!customerExists)
                {
                    return NotFound();
                }
                else
                {
                    //var responseMsg = Request.CreateErrorResponse(HttpStatusCode.Conflict, 
                    //                        "The content of the customer has changed since"+
                    //                                  " you last accessed it. Do you want to reload the page?");
                    return Content<string>(HttpStatusCode.Conflict,
                                            "The content of the customer has changed since" +
                                                      " you last accessed it. Do you want to reload the page?");
                   // return ResponseMessage(responseMsg);
                }
            }
            catch (DbUpdateException)
            {
                return InternalServerError();
                //throw;
            }

            //return StatusCode(HttpStatusCode.NoContent);
            return Ok(customerDto);
        }

        // POST: api/Branches
        [ResponseType(typeof(Customer))]
        public async Task<IHttpActionResult> PostCustomer(CustomerDto customerDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var customer = Mapper.Map<Customer>(customerDto);
            try
            {
                await _customerRepository.AddNewCustomer(customer);
            }
            catch (DbUpdateException)
            {
                return InternalServerError();
                throw;
            }

            return CreatedAtRoute("DefaultApi", new { id = customer.Id }, customer);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _customerRepository.Dispose();
            }
            base.Dispose(disposing);
        }

        //private bool BranchExists(int id)
        //{
        //    return db.Branches.Count(e => e.Id == id) > 0;
        //}


    }
}