﻿namespace CBA.Core.Entities
{
    public abstract class Person : BaseEntity
    {
        public virtual string FirstName { get; set; }
        
        public virtual string LastName { get; set; }
        
        public virtual string OtherNames { get; set; }
        
        public string PhoneNumber { get; set; }
        
        public string Email { get; set; }
        public string Address { get; set; }

    }
}
