﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CBA.Core.Entities
{
    public class CustomerAccount : Account
    {
        public int CustomerId { get; set; }
        public Customer Customer { get; set; }
       // public virtual String Name { get; set; }
        //[Display(Name = "Account Number")]
        public long AccountNumber { get; set; }
        public int BranchId { get; set; }
        public Branch Branch { get; set; }
        public AccountType AccountType { get; set; }
        public bool IsAccountActive { get; set; }
        //public virtual double Balance { get; set; }
        public double AccumulatedInterest { get; set; }
        public double AccumulatedCharge { get; set; }// e.g COT, atm fees etc
        public DateTime LastInterestPaidDate { get; set; }

    }
}
