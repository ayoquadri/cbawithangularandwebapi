﻿using System.Collections.Generic;

namespace CBA.Core.Entities
{
    public  class Branch : BaseEntity
    {
        
        //public virtual string Code { get; set; }
        
        public string Name { get; set; }
          
        public Status Status { get; set; }
        
        public bool IsBranchOpen { get; set; }
               
        public string Address { get; set; }
        
       // public DateTime FinancialDate { get; set; }

        public virtual ICollection<ApplicationUser> Users { get; set; }
    }
}
