﻿namespace CBA.Core.Entities
{
    public class Account : BaseEntity
    {
        
        public string Name { get; set; }        
        public double Balance { get; set; }
    }
}
