﻿using System;

namespace CBA.Core.Entities
{
    public abstract class BaseEntity
    {
        public int Id { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime DateModified { get; set; }

        public byte[] RowVersion { get; set; }

        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }

    }
}
