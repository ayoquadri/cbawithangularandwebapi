﻿//using Microsoft.AspNet.Identity.EntityFramework;

using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace CBA.Core.Entities
{
    public class ApplicationUser : IdentityUser
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string OtherNames { get; set; }

        public int BranchId { get; set; }

        public Branch Branch { get; set; }

        //public DateTime DateCreated { get; set; }

       // public DateTime DateModified { get; set; }

        public byte[] RowVersion { get; set; }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager, string authenticationType)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, authenticationType);
            // Add custom user claims here
            return userIdentity;
        }
      
    }

    


}
