﻿using System.Linq;
using CBA.Core.DTOs;
using CBA.Core.Entities;

namespace CBA.Core.Persitence.Repositories
{
    public interface IBranchRepository : IRepository<Branch>
    {
        Branch GetBranchByName(string name);
        IQueryable<BranchDto> GetQueryableBranches(string searchTerm=null);
    }
    
}
