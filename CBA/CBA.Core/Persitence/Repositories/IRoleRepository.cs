﻿using System.Collections.Generic;
using CBA.Core.Entities;
using Microsoft.AspNet.Identity.EntityFramework;

namespace CBA.Core.Persitence.Repositories
{
    public interface IRoleRepository : IRepository<Role>
    {
        List<IdentityRole> GetAllRoles();
    }
    
}
