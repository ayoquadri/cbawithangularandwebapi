﻿using System.Linq;
using System.Threading.Tasks;
using CBA.Core.Entities;

namespace CBA.Core.Persitence.Repositories
{
    public interface ICustomerAccountRepository : IRepository<CustomerAccount>
    {
        IQueryable GetQueryableCustomerAccounts(string searchTerm);
        Task AddNewCustomerAccount(CustomerAccount customer);
        Task<CustomerAccount> UpdateCustomerAccount(int id,CustomerAccount entity);
        Task<CustomerAccount> GetCustomerAccount(int id);
    }
    
}
