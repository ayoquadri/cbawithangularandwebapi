﻿using System.Linq;
using System.Threading.Tasks;
using CBA.Core.Entities;

namespace CBA.Core.Persitence.Repositories
{
    public interface IUserRepository : IRepository<ApplicationUser>
    {
        IQueryable GetQueryableUsers(string searchTerm);
        Task<ApplicationUser> GetUser(string id);
        Task<ApplicationUser> DetachAndUpdate(string id,ApplicationUser user);
    }
    
}
