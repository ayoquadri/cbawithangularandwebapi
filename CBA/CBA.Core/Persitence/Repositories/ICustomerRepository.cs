﻿using System.Linq;
using System.Threading.Tasks;
using CBA.Core.DTOs;
using CBA.Core.Entities;

namespace CBA.Core.Persitence.Repositories
{
    public interface ICustomerRepository : IRepository<Customer>
    {
        IQueryable GetQueryableCustomers(string searchTerm);
        Task AddNewCustomer(Customer customer);
        Task<Customer> UpdateCustomer(int id,Customer entity);
        IQueryable<CustomerDto> FindCustomers(string search);
    }
    
}
