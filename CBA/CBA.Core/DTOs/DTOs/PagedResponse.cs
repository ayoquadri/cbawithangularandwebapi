﻿using System.Collections.Generic;

namespace FinSky.WebAPI.Models.DTOs
{
    public class PagedResponse<T>
    {
        public int Total { get; set; }
        public double TotalPages { get; set; }
        public List<T> Data { get; set; }
    }
}