﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using FinSky.Core.Entities;
using Newtonsoft.Json;

namespace FinSky.WebAPI.Models.DTOs
{
    public class BranchDTO
    {
        
        public virtual int Id { get; set; }

        [Display(Name = "Branch Name")]
        [Required(ErrorMessage = "{0} is required")]
        [StringLength(50, ErrorMessage = "{0} should not exceed {1} characters")]
        [RegularExpression(@"^[a-zA-Z]+( [a-zA-z0-9]+)*$", ErrorMessage = "{0} should consist of only alphabets, space and number")]
        public virtual string Name { get; set; }

        public virtual bool IsBranchOpen { get; set; }

        [Required]
        [StringLength(200)]
        public string Address { get; set; }

       
    }

}