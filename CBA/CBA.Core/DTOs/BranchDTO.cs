﻿using System.ComponentModel.DataAnnotations;

namespace CBA.Core.DTOs
{
    public class BranchDto
    {
        public int Id { get; set; }

        [Display(Name = "Branch Name")]
        [Required(ErrorMessage = "{0} is required")]
        [StringLength(50, ErrorMessage = "{0} should not exceed {1} characters")]
        [RegularExpression(@"^[a-zA-Z0-9-\s]+$", 
                        ErrorMessage = "{0} should consist of only alphabets, space and number")]
        public string Name { get; set; }

        public bool IsBranchOpen { get; set; }

        [Required]
        [Display(Name = "Branch Address")]
        [StringLength(200, ErrorMessage = "{0} should not exceed {1} characters")]
        [RegularExpression(@"^[a-zA-Z0-9]+[a-zA-Z0-9\s-,\.]*",
                        ErrorMessage = "{0} should have an alphabet or number")]
        public string Address { get; set; }
    }

}