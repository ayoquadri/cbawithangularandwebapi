﻿using System.ComponentModel.DataAnnotations;

namespace CBA.Core.DTOs
{
    public class UserDto
    {
        public string Id { get; set; }

        [Required]
        [Display(Name = "UserName")]
        [StringLength(50, ErrorMessage = "{0} should not exceed {1} characters")]
        public string UserName { get; set; }

        [Required]
        [Display(Name ="Email Address")]
        [StringLength(50, ErrorMessage = "{0} should not exceed {1} characters")]
        [RegularExpression(@"^(?i)[a-z](?:\w|\.)*@((?:[a-z0-9]+[-.]?[a-z0-9]+)+)(?:\.[a-z]+)$",
            ErrorMessage ="{0} is Invalid. Enter e.g ayo@gmail.com")]
        public string Email { get; set; }

        [Display(Name = "PhoneNumber")]
        [Required]
        [RegularExpression(@"^(?:\+?234|0){1}[7-9]\d[1-9]\d{7}$",
                                ErrorMessage = "{0} is invalid. Enter e.g 07036833337,+2347036833337")]
        public string PhoneNumber { get; set; }

        [Required]
        [Display(Name = "First Name")]
        [StringLength(50, ErrorMessage = "{0} should not exceed {1} characters")]
        [RegularExpression(@"^[a-zA-Z-]+$")]
        public string FirstName { get; set; }

        //TODO: Get the roles the user belongs too
        [Required]
        [Display(Name = "Last Name")]
        [RegularExpression(@"^[a-zA-Z-]+$")]
        [StringLength(50, ErrorMessage = "{0} should not exceed {1} characters")]
        public string LastName { get; set; }


        [Display(Name = "Other Names")]
        [RegularExpression(@"^[a-zA-Z-\s]+$")]
        [StringLength(50, ErrorMessage = "{0} should not exceed {1} characters")]
        public string OtherNames { get; set; }

        [Required]
        [Display(Name = "Branch Name")]
        [RegularExpression(@"^[a-zA-Z0-9-\s]+$")]
        [StringLength(50, ErrorMessage = "{0} should not exceed {1} characters")]
        public string BranchName { get; set; }

        [Required]
        public int? BranchId { get; set; }


    }

}