﻿using System.ComponentModel.DataAnnotations;

namespace CBA.Core.DTOs
{
    public class CustomerAccountDto
    {
        public int Id { get; set; }
        public string Name { get; set; }

        [Display(Name = "Customer Id")]
        [Required]
        public int? CustomerId { get; set; }
        public int AccountType { get; set; }
        public long AccountNumber { get; set; }
        public string CustomerFirstName { get; set; }
        public string CustomerCustomerId { get; set; }
        public string CustomerLastName { get; set; }
        public string CustomerOtherNames { get; set; }
        public int BranchId { get; set; }
        public string BranchName { get; set; }
        public double Balance { get; set; }
        public bool IsActive { get; set; }

        
    }

}