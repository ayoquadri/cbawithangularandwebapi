﻿using Moq;
using NUnit.Framework;
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Results;
using CBA.Core.DTOs;
using CBA.Core.Entities;
using CBA.Core.Persitence.Repositories;
using CBA.WebAPI.Controllers;

namespace CBA.Tests.ApiControllers
{
    [TestFixture]
    public class BranchesControllerTests
    {
        private readonly int TEST_ID = 22;
        private Mock<IBranchRepository> mockBranchRepository;
        private BranchesController controller;

        [SetUp]
        public void SetUp()
        {
            mockBranchRepository = new Mock<IBranchRepository>();
            controller = new BranchesController(mockBranchRepository.Object);
            controller.Configuration = new HttpConfiguration();
        }

        [Test]
        public async Task GetBranch_ShouldReturnCorrectBranch()
        {
            mockBranchRepository.Setup(x => x.Get(TEST_ID)).Returns(
                    Task.FromResult<Branch>(new Branch() { Id = TEST_ID})
                );
            
            IHttpActionResult actionResult = await controller.GetBranch(TEST_ID);

            var contentResult = actionResult as OkNegotiatedContentResult<BranchDto>;

            Assert.IsNotNull(contentResult);
            Assert.IsNotNull(contentResult.Content);
            Assert.AreEqual(TEST_ID, contentResult.Content.Id);
        }

        [Test]
        public async Task GetBranch_ShouldReturnNotFound()
        {
            mockBranchRepository.Setup(x => x.Get(TEST_ID)).Returns(
                    Task.FromResult<Branch>(null)
                );

            IHttpActionResult actionResult = await controller.GetBranch(TEST_ID);

            Assert.IsInstanceOf(typeof(NotFoundResult), actionResult);
        }

        [Test]
        public async Task PutBranch_ShouldReturnBadRequest()
        {
            var branchDto = new BranchDto
            {
                Id = 100//different from TEST_ID
            };
            IHttpActionResult actionResult = await controller.PutBranch(TEST_ID, branchDto);

            Assert.IsInstanceOf(typeof(BadRequestResult), actionResult);
        }
        [Test]
        public async Task PutBranch_ShouldReturnNotFound()
        {
            mockBranchRepository.Setup(x => x.DetachAndUpdate(It.IsAny<int>(),It.IsAny<Branch>()))
                                .Returns(Task.FromResult<Branch>(null)
                );

            var branchDto = new BranchDto
            {
                Id = TEST_ID //same id 
            };

            IHttpActionResult actionResult = await controller.PutBranch(TEST_ID, branchDto);

            Assert.IsInstanceOf(typeof(NotFoundResult), actionResult);
        }
        [Test]
        public async Task PutBranch_ShouldReturn_InvalidModelState_RequiredFields()
        {
            var branchDto = GetDemoBranch();

            controller.Validate(branchDto);

            IHttpActionResult actionResult = await controller.PutBranch(TEST_ID, branchDto);
            var invalidModelStateResult = actionResult as InvalidModelStateResult;

            var props = typeof(BranchDto).GetProperties().Where(
                prop => Attribute.IsDefined(prop, typeof(RequiredAttribute)));

            Assert.That(invalidModelStateResult, Is.Not.Null);

            Assert.That(invalidModelStateResult.ModelState.Keys.Count, Is.EqualTo(props.Count()));
        }

        [Test]
        public async Task PutBranch_ShouldReturnOk()
        {
            mockBranchRepository.Setup(x => x.DetachAndUpdate(It.IsAny<int>(), It.IsAny<Branch>()))
                                .Returns(Task.FromResult<Branch>(new Branch())
                );
            
            IHttpActionResult actionResult = await controller.PutBranch(TEST_ID, GetDemoBranch());

            Assert.IsInstanceOf(typeof(OkNegotiatedContentResult<BranchDto>), actionResult);
        }
        [Test]
        public async Task PostBranch_ShouldReturnCreated()
        {
            int autoGenId = new Random().Next();

            mockBranchRepository.Setup(x => x.Save(It.IsAny<Branch>()))
                                .Returns<Branch>(b => {
                                      b.Id = autoGenId;
                                      return Task.FromResult(b);
                                  } );
                                 
            IHttpActionResult actionResult = await controller.PostBranch(GetDemoBranch());
            var createdResult = actionResult as CreatedAtRouteNegotiatedContentResult<BranchDto>;

            Assert.IsNotNull(createdResult);
            Assert.AreEqual("DefaultApi",createdResult.RouteName);
            Assert.AreEqual(autoGenId,createdResult.Content.Id);
            Assert.AreEqual(autoGenId,createdResult.RouteValues["id"]); 
        }

        private BranchDto GetDemoBranch()
        {
            var branchDto = new BranchDto
            {
                Id = TEST_ID //same id 
            };
            return branchDto;
        }

    }
}
