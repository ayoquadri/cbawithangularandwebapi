﻿using Moq;
using NUnit.Framework;
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Results;
using CBA.Core.DTOs;
using CBA.Core.Entities;
using CBA.Core.Persitence.Repositories;
using CBA.WebAPI.Controllers;

namespace CBA.Tests.ApiControllers
{
    [TestFixture]
    public class UsersControllerTests
    {
        private readonly string TEST_ID = "22";
        private Mock<IUserRepository> mockUserRepository;
        private Mock<IBranchRepository> mockBranchRepository;
        private Mock<IRoleRepository> mockRoleRepository;
        private UsersController controller;

        [SetUp]
        public void SetUp()
        {
            Console.WriteLine("SetUp>>>>>>>>>>>>>>>>>");
            mockUserRepository = new Mock<IUserRepository>();
            mockBranchRepository = new Mock<IBranchRepository>();
            mockRoleRepository = new Mock<IRoleRepository>();

            controller = new UsersController(mockUserRepository.Object,
                mockBranchRepository.Object,
                mockRoleRepository.Object);

            controller.Configuration = new HttpConfiguration();
        }

        [Test]
        public async Task GetUser_ShouldReturnCorrectUser()
        {
            mockUserRepository.Setup(x => x.GetUser(TEST_ID)).Returns(
                Task.FromResult<ApplicationUser>(new ApplicationUser() {Id = TEST_ID})
                );

            IHttpActionResult actionResult = await controller.GetUser(TEST_ID);

            var contentResult = actionResult as OkNegotiatedContentResult<UserDto>;

            Assert.IsNotNull(contentResult);
            Assert.IsNotNull(contentResult.Content);
            Assert.AreEqual(TEST_ID, contentResult.Content.Id);
        }

        [Test]
        public async Task GetUser_ShouldReturnNotFound()
        {
            mockUserRepository.Setup(x => x.GetUser(TEST_ID)).Returns(
                Task.FromResult<ApplicationUser>(null)
                );

            IHttpActionResult actionResult = await controller.GetUser(TEST_ID);

            Assert.IsInstanceOf(typeof(NotFoundResult), actionResult);
        }

        [Test]
        public async Task PutUser_ShouldReturnBadRequest()
        {
            var user = new UserDto
            {
                Id = "100", //different from TEST_ID
                Email = "aaa"
            };
            IHttpActionResult actionResult = await controller.PutUser(TEST_ID, user);

            Assert.IsInstanceOf(typeof(BadRequestResult), actionResult);
        }

        [Test]
        public async Task PutUser_ShouldReturnNotFound()
        {
            mockUserRepository.Setup(x => x.DetachAndUpdate(It.IsAny<string>(), It.IsAny<ApplicationUser>()))
                .Returns(Task.FromResult<ApplicationUser>(null)
                );

            var user = new UserDto
            {
                Id = TEST_ID //same id 
            };

            IHttpActionResult actionResult = await controller.PutUser(TEST_ID, user);

            Assert.IsInstanceOf(typeof(NotFoundResult), actionResult);
        }

        [Test]
        public async Task PutUser_ShouldReturnOk()
        {
            mockUserRepository.Setup(x => x.DetachAndUpdate(It.IsAny<string>(), It.IsAny<ApplicationUser>()))
                .Returns(Task.FromResult<ApplicationUser>(new ApplicationUser())
                );

            var user = new UserDto
            {
                Id = TEST_ID //same id 
            };

            IHttpActionResult actionResult = await controller.PutUser(TEST_ID, user);

            Assert.IsInstanceOf(typeof(OkNegotiatedContentResult<UserDto>), actionResult);
        }

        [Test]
        public async Task PutUser_ShouldReturn_InvalidModelState_RequiredFields()
        {
            var userDto = GetDemoUser();
            
            controller.Validate(userDto);

            IHttpActionResult actionResult = await controller.PutUser(TEST_ID, userDto);
            var invalidModelStateResult = actionResult as InvalidModelStateResult;

            var props = typeof(UserDto).GetProperties().Where(
                prop => Attribute.IsDefined(prop, typeof(RequiredAttribute)));

            Assert.That(invalidModelStateResult, Is.Not.Null);

            Assert.That(invalidModelStateResult.ModelState.Keys.Count, Is.EqualTo(props.Count()));
        }

        [Test]
        public async Task PutUser_ShouldAllowValidEmail(
            [Values("ayodeji@gmail.com","joke@yahoo.com.ng")] string email)
        {
            var userDto = GetDemoUser();
            userDto.Email = email;
            //controller.Configuration = new HttpConfiguration();
            controller.Validate(userDto);

            IHttpActionResult actionResult = await controller.PutUser(TEST_ID, userDto);
            var invalidModelStateResult = actionResult as InvalidModelStateResult;
            
            Assert.That(invalidModelStateResult, Is.Not.Null);

            Assert.That(invalidModelStateResult
                                .ModelState.Keys
                                    .Contains(nameof(userDto.Email)), Is.False);
        }

        [Test]
        public async Task PutUser_ShouldAllowValidPhoneNumber(
            [Values("07036933337","+2347036933337","2348053334445")]string phoneNumeber)
        {
            var userDto = GetDemoUser();
            userDto.PhoneNumber = phoneNumeber;
            //controller.Configuration = new HttpConfiguration();
            controller.Validate(userDto);

            IHttpActionResult actionResult = await controller.PutUser(TEST_ID, userDto);
            var invalidModelStateResult = actionResult as InvalidModelStateResult;

            Assert.That(invalidModelStateResult, Is.Not.Null);

            Assert.That(invalidModelStateResult
                                .ModelState.Keys
                                    .Contains(nameof(userDto.PhoneNumber)), Is.False);
        }

        private UserDto GetDemoUser()
        {
            var user = new UserDto
            {
                Id = "100"
            };
            return user;
        }
    }

}

