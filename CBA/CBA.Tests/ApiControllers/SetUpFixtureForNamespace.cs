﻿using NUnit.Framework;
using CBA.WebAPI;

namespace CBA.Tests.ApiControllers
{
    [SetUpFixture]
    public class SetUpFixtureForNamespace
    {
        [OneTimeSetUp]
        public void SetUp() {
            AutoMapperConfig.RegisterMappings();
        }                                                                               
    }
}
