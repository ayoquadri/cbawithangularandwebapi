﻿using System.ComponentModel.Composition;
using CBA.Core.Entities;

namespace CBA.Persistence.Configurations
{
    [Export(typeof(IEntityConfiguration))]
    internal class PersonConfiguration<T> : BaseEntityConfiguration<T>, IEntityConfiguration
        where T : Person
    {
        public PersonConfiguration()
        {
            Property(u => u.FirstName)
                .IsRequired();

            Property(u => u.LastName)
                .IsRequired();

            Property(u => u.Email)
                .IsRequired();

            Property(u => u.PhoneNumber)
                .IsRequired();

            Property(u => u.Address)
                .IsRequired();
            //Property(c => c.Name)
            //    .IsRequired()
            //    .HasMaxLength(100)
            //    .HasColumnAnnotation("Index", new IndexAnnotation(new IndexAttribute() {IsUnique = true}));

            //Property(c => c.Address)
            //    .IsRequired()
            //    .HasMaxLength(200);

            //HasMany(c => c.Users);

        }
        
       
    }
}
