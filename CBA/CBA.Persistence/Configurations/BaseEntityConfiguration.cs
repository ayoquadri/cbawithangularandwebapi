﻿using System.Data.Entity.ModelConfiguration;
using System.Data.Entity.ModelConfiguration.Configuration;
using CBA.Core.Entities;

namespace CBA.Persistence.Configurations
{
    internal abstract class BaseEntityConfiguration<T> : EntityTypeConfiguration<T> where T : BaseEntity
    {
        protected BaseEntityConfiguration()
        {
            HasKey(c => c.Id);
          //  Property(c => c.RowVersion).IsRowVersion();
        }

        public void AddConfiguration(ConfigurationRegistrar registrar)
        {
            registrar.Add(this);
        }
    }
}
