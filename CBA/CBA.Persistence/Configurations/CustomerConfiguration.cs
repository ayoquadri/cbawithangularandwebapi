﻿using System.ComponentModel.Composition;
using CBA.Core.Entities;

namespace CBA.Persistence.Configurations
{
    [Export(typeof(IEntityConfiguration))]
    internal class CustomerConfiguration : PersonConfiguration<Customer>
    {
        public CustomerConfiguration()
        {
            Property(c => c.CustomerID)
                .IsRequired();
        }
        
    }
}
