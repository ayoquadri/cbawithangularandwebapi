﻿using System.ComponentModel.Composition;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using CBA.Core.Entities;

namespace CBA.Persistence.Configurations
{
    [Export(typeof(IEntityConfiguration))]
    internal class BranchConfiguration : BaseEntityConfiguration<Branch>, IEntityConfiguration
    {
        public BranchConfiguration()
        {
            HasKey(c => c.Id);
            Property(c => c.Name)
                .IsRequired()
                .HasMaxLength(100)
                .HasColumnAnnotation("Index", new IndexAnnotation(new IndexAttribute() {IsUnique = true}));

            Property(c => c.Address)
                .IsRequired()
                .HasMaxLength(200);

            HasMany(c => c.Users);
            

        }
        
        
    }
}
