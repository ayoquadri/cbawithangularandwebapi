﻿using System.ComponentModel.Composition;
using CBA.Core.Entities;

namespace CBA.Persistence.Configurations
{
    [Export(typeof(IEntityConfiguration))]
    internal class CustomerAccountConfiguration : AccountConfiguration<CustomerAccount>
    {
        public CustomerAccountConfiguration()
        {
            Property(c => c.AccountNumber)
                .IsRequired();

            Property(c => c.AccountType)
                .IsRequired();

            Property(c => c.AccumulatedCharge)
                .IsRequired();

            Property(c => c.AccumulatedInterest)
                .IsRequired();

            Property(c => c.LastInterestPaidDate)
               .IsRequired(); 
            
            HasRequired(c => c.Branch);
            HasRequired(c => c.Customer);
        } 
        
    }
}
