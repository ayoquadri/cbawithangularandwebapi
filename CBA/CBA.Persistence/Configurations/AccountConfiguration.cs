﻿using System.ComponentModel.Composition;
using CBA.Core.Entities;

namespace CBA.Persistence.Configurations
{
    [Export(typeof(IEntityConfiguration))]
    internal class AccountConfiguration<T> : BaseEntityConfiguration<T>, IEntityConfiguration
        where T : Account
    {
        public AccountConfiguration()
        {
            Property(u => u.Name)
                .IsRequired();

            Property(u => u.Balance)
                .IsRequired();

        }
        
       
    }
}
