﻿using System.Collections.Generic;
using System.ComponentModel.Composition;

namespace CBA.Persistence.Configurations
{
    internal class ContextConfiguration
    {
        [ImportMany(typeof(IEntityConfiguration))]
        public IEnumerable<IEntityConfiguration> Configurations { get; set; }
    }
}
