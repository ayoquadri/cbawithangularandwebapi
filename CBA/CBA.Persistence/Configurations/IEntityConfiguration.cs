﻿using System.Data.Entity.ModelConfiguration.Configuration;

namespace CBA.Persistence.Configurations
{
    public interface IEntityConfiguration
    {
        void AddConfiguration(ConfigurationRegistrar registrar);
    }
}
