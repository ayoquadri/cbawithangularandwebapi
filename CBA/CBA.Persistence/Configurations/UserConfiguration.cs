﻿using System.ComponentModel.Composition;
using System.Data.Entity.ModelConfiguration;
using System.Data.Entity.ModelConfiguration.Configuration;
using CBA.Core.Entities;

namespace CBA.Persistence.Configurations
{
    [Export(typeof(IEntityConfiguration))]
    internal class UserConfiguration : EntityTypeConfiguration<ApplicationUser>, IEntityConfiguration
    { 
        public UserConfiguration()
        {
            Property(u => u.FirstName)
                .IsRequired()
                .HasMaxLength(100);

            Property(u => u.LastName)
                .IsRequired();

            Property(u => u.RowVersion)
                .IsRowVersion();

            HasRequired(u => u.Branch);

        }
        public void AddConfiguration(ConfigurationRegistrar registrar)
        {
            registrar.Add(this);
        }

    }
}
