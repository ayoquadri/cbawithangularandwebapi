﻿using System;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Data.Entity;
using System.Reflection;
using System.Threading.Tasks;
using CBA.Core.Entities;
using CBA.Persistence.Configurations;
using Microsoft.AspNet.Identity.EntityFramework;

namespace CBA.Persistence
{
    //public class CBAContext : DbContext
    //{
    //    public CBAContext() : base("name=CBAContext")
    //    {
    //        this.Database.Log = s => System.Diagnostics.Debug.WriteLine(s);
    //    }

    //    public DbSet<Branch> Branches { get; set; }
    //    public DbSet<ApplicationUser> Users { get; set; }
    //    public DbSet<Account> Accounts { get; set; }
    //    public DbSet<CustomerAccount> CustomerAccounts { get; set; }

    //    protected override void OnModelCreating(DbModelBuilder modelBuilder)
    //    {

    //        //base.OnModelCreating(modelBuilder); 
    //    }
    //}

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public DbSet<Branch> Branches { get; set; }
        public DbSet<Customer> Customers { get; set; }
        //public DbSet<ApplicationUser> Users { get; set; }
        //public DbSet<Account> Accounts { get; set; }
        public DbSet<CustomerAccount> CustomerAccounts { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            var contextConfiguration = new ContextConfiguration();
            var catalog =  new AssemblyCatalog(Assembly.GetExecutingAssembly());
            var container = new CompositionContainer(catalog);

            //this line scans the whole assembly for type that inherits from IEntityConfiguration
            //and poulate the IEnumerable with all of them
            container.ComposeParts(contextConfiguration);

            foreach (var configuration in contextConfiguration.Configurations)
            {
                configuration.AddConfiguration(modelBuilder.Configurations);
            }

            base.OnModelCreating(modelBuilder);
        }

        public override Task<int> SaveChangesAsync()
        {
            DoSaveChanges();
            return base.SaveChangesAsync();
        }
        public override int SaveChanges()
        {
            DoSaveChanges();
            return base.SaveChanges();
        }

        private void DoSaveChanges()
        {
            foreach (var entry in ChangeTracker.Entries<BaseEntity>())
            {
                if (entry.State == EntityState.Added)
                {
                    entry.Entity.DateCreated = entry.Entity.DateModified = DateTime.Now;
                }
                else if (entry.State == EntityState.Modified)
                {
                    // Ignore the DateCreated updates on Modified entities. 
                    entry.Entity.DateCreated=entry.Entity.DateModified = DateTime.Now;
                    //entry.Property("DateCreated").IsModified = false;
                }

            }

        }
    }
}
