using System.Data.Entity.Migrations;

namespace CBA.Persistence.Migrations
{
    public partial class isAccountActiveColumn : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CustomerAccounts", "IsAccountActive", c => c.Boolean(nullable: false));
            AlterColumn("dbo.CustomerAccounts", "Name", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.CustomerAccounts", "Name", c => c.String());
            DropColumn("dbo.CustomerAccounts", "IsAccountActive");
        }
    }
}
