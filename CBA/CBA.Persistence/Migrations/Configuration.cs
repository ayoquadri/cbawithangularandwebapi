using System.Data.Entity.Migrations;
using System.Linq;
using CBA.Core.Entities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace CBA.Persistence.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(ApplicationDbContext context)
        {
            context.Branches.AddOrUpdate(b => b.Name,
                 new Branch { Name = "Head Office", IsBranchOpen = true, Status = Status.ACTIVE, Address = "Victoria Island" },
                 new Branch { Name = "Abuja Branch", IsBranchOpen = true, Status = Status.ACTIVE, Address = "Wuse Abuja" },
                 new Branch { Name = "Lekki Branch", IsBranchOpen = false, Status = Status.ACTIVE, Address = "Lekki Phase 1" },
                 new Branch { Name = "Ibadan Branch", IsBranchOpen = true, Status = Status.ACTIVE, Address = "Ibadan,Oyo state" }
             );
            context.SaveChanges();

            InitializeIdentityForEF(context);
            base.Seed(context);
        }
        public static void InitializeIdentityForEF(ApplicationDbContext context)
        {
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
            const string name = "JohnDoe@CBA.com";
            const string password = "P@ssword1";
            const string roleName = "Super Admin";
            const string roleName2 = "Teller";

            //Create Role Admin if it does not exist
            var role = roleManager.FindByName(roleName);
            if (role == null)
            {
                role = new IdentityRole(roleName);
                var roleresult = roleManager.Create(role);
            }
            var role2 = roleManager.FindByName(roleName2);
            if (role2 == null)
            {
                role2 = new IdentityRole(roleName2);
                var roleresult = roleManager.Create(role2);
            }

            var user = userManager.FindByName(name);
            if (user == null)
            {
                Branch branch;

                branch = context.Branches.First(x => x.Name.Equals("Head Office"));

                user = new ApplicationUser { UserName = name, Email = name, BranchId = branch.Id,
                                FirstName = "johnson", LastName = "gabriel"};
                var result = userManager.Create(user, password);
                result = userManager.SetLockoutEnabled(user.Id, false);
            }

            // Add user admin to Role Admin if not already added
            var rolesForUser = userManager.GetRoles(user.Id);
            if (!rolesForUser.Contains(role.Name))
            {
                var result = userManager.AddToRole(user.Id, role.Name);
            }
        }
    }
}
