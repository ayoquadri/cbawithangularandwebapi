using System.Data.Entity.Migrations;

namespace CBA.Persistence.Migrations
{
    public partial class removeautogenerationCustomerId : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.CustomerAccounts", "CustomerId", "dbo.Customers");
            DropPrimaryKey("dbo.Customers");
            AlterColumn("dbo.Customers", "Id", c => c.Int(nullable: false, identity: true));
            AlterColumn("dbo.Customers", "CustomerID", c => c.Int(nullable: false));
            AddPrimaryKey("dbo.Customers", "Id");
            AddForeignKey("dbo.CustomerAccounts", "CustomerId", "dbo.Customers", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.CustomerAccounts", "CustomerId", "dbo.Customers");
            DropPrimaryKey("dbo.Customers");
            AlterColumn("dbo.Customers", "CustomerID", c => c.Int(nullable: false, identity: true));
            AlterColumn("dbo.Customers", "Id", c => c.Int(nullable: false));
            AddPrimaryKey("dbo.Customers", "Id");
            AddForeignKey("dbo.CustomerAccounts", "CustomerId", "dbo.Customers", "Id", cascadeDelete: true);
        }
    }
}
