﻿using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper.QueryableExtensions;
using CBA.Core.DTOs;
using CBA.Core.Entities;
using CBA.Core.Persitence.Repositories;

namespace CBA.Persistence.Repositories
{
    //suppose to be internal
    public class UserRespository : AbstractRepository<ApplicationUser,ApplicationDbContext>, IUserRepository
    {
        public async Task<ApplicationUser> DetachAndUpdate(string id, ApplicationUser user)
        {
            var entity = await GetUser(id);
            
            if (entity == null) //Invalid id.. an id that do not exist in the database
                return null;

            DataContext.Entry(entity).CurrentValues.SetValues(user);
            await DataContext.SaveChangesAsync();
            return entity;
        }

        public IQueryable GetQueryableUsers(string searchTerm)
        {
            if (!string.IsNullOrWhiteSpace(searchTerm))
            {
                return DataContext.Users
                                    .Where(u => u.FirstName.Contains(searchTerm) || u.UserName.Contains(searchTerm)
                                                        || u.LastName.Contains(searchTerm)
                                                    || u.OtherNames.Contains(searchTerm) || u.Branch.Name.Contains(searchTerm))
                                    .ProjectTo<UserDto>();
            }
            return DataContext.Set<ApplicationUser>().Include(u =>u.Branch).ProjectTo<UserDto>();
        }

        public async Task<ApplicationUser> GetUser(string id)
        {
            return await DataContext.Set<ApplicationUser>().Include(u => u.Branch)
                                    .FirstOrDefaultAsync(u=>u.Id == id);
        }

        
    }
}
