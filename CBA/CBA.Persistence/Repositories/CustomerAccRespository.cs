﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper.QueryableExtensions;
using CBA.Core.DTOs;
using CBA.Core.Entities;
using CBA.Core.Persitence.Repositories;

namespace CBA.Persistence.Repositories
{
    //suppose to be internal
    public class CustomerAccountRepository : AbstractRepository<CustomerAccount, ApplicationDbContext>,
        ICustomerAccountRepository
    {
        public IQueryable GetQueryableCustomerAccounts(string searchTerm)
        {
            if (!string.IsNullOrWhiteSpace(searchTerm))
            {
                return DataContext.CustomerAccounts
                                    .Where(c => c.Name.Contains(searchTerm) || c.AccountNumber.ToString().Contains(searchTerm)
                                                        || c.Branch.Name.Contains(searchTerm))
                                    .ProjectTo<CustomerAccountDto>();
            }
            return GetAll().ProjectTo<CustomerAccountDto>();
        }


        public async Task AddNewCustomerAccount(CustomerAccount customerAcc)
        {
            customerAcc.AccountNumber = GenerateAccountNumber(customerAcc.AccountType,
                                                        customerAcc.CustomerId, customerAcc.BranchId);
            customerAcc.IsAccountActive = true;
            customerAcc.LastInterestPaidDate = DateTime.Now;

            await Save(customerAcc);
        }

        public async Task<CustomerAccount> UpdateCustomerAccount(int id,CustomerAccount entity)
        {
            var customerAcc = await Get(id);
            if (customerAcc == null) return null;
            customerAcc.Name = entity.Name;
            customerAcc.BranchId = entity.BranchId;
            await DataContext.SaveChangesAsync();
            return customerAcc;
        }

        public async Task<CustomerAccount> GetCustomerAccount(int id)
        {
            return await DataContext.Set<CustomerAccount>().Include(c => c.Customer).Include(c=>c.Branch)
                                    .FirstOrDefaultAsync(u => u.Id == id);
        }

        private long GenerateAccountNumber(AccountType accountType, int customerId, int branchID)
        {
            int institutionID = 1;
            StringBuilder sb = new StringBuilder();
            string branchCode = branchID.ToString("D2");
            int numberOfCustomerAccountWithAccountType = GetNumberOfCustomerAccountWithAccountType(customerId, accountType);
            int a = (int)accountType;
            sb.AppendFormat("{0}{1}{2}{3}{4}", institutionID,
                                        branchID.ToString("D2"),
                                             a,
                                             numberOfCustomerAccountWithAccountType,
                                             customerId.ToString("D6"));
            return long.Parse(sb.ToString());

        }

        private int GetNumberOfCustomerAccountWithAccountType(int customerID, AccountType accountType)
        {
            return Count(c => c.CustomerId == customerID && c.AccountType == accountType);
        }
    }
}

    

