﻿using System.Collections.Generic;
using System.Linq;
using CBA.Core.Entities;
using CBA.Core.Persitence.Repositories;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace CBA.Persistence.Repositories
{
    //suppose to be internal
    public class RoleRespository : AbstractRepository<Role,ApplicationDbContext>, IRoleRepository
    {
        private RoleManager<IdentityRole> _roleManager;
            //new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(DataContext));

        public RoleRespository()
        {
            _roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(DataContext));
        }
        
        public List<IdentityRole> GetAllRoles()
        {
           return  _roleManager.Roles.ToList();

        }

    }
}
