﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace CBA.Persistence.Repositories
{
    //this is suppose to be internal
    public abstract class AbstractRepository<TEntity, TContext>
        where TEntity : class
        where TContext : DbContext, new()
    {
        private TContext _dataContext;
       // protected TContext DataContext => new TContext();

        protected TContext DataContext
        {
            get {
                if (_dataContext != null)
                    return _dataContext;

                _dataContext = new TContext();
                return _dataContext;
            }
            set { }
        }
        public async Task<TEntity> Get(int id)
        {
            return await DataContext.Set<TEntity>().FindAsync(id);
        }

        public async Task Save(TEntity entity)
        {
            DataContext.Entry<TEntity>(entity).State = EntityState.Added;
            await DataContext.SaveChangesAsync();
        }

        public async Task Update(TEntity entity)
        {
            DataContext.Entry<TEntity>(entity).State = EntityState.Modified;
            await DataContext.SaveChangesAsync();
        }

        public async Task<TEntity> DetachAndUpdate(int id, TEntity modifiedEntity)
        {
            var entity = await Get(id);
            if (entity == null) //Invalid id.. an id that do not exist in the database
                return null;
            DataContext.Entry(entity).CurrentValues.SetValues(modifiedEntity);

            await DataContext.SaveChangesAsync();

            return entity;
        }

        public async Task<List<TEntity>> Filter(Expression<Func<TEntity, bool>> predicate)
        {
            return await DataContext.Set<TEntity>().Where(predicate).ToListAsync();
        }

        public IQueryable<TEntity> GetAll()
        {
            return DataContext.Set<TEntity>();
        }
        public int Count(Expression<Func<TEntity, bool>> expression)
        {
            return DataContext.Set<TEntity>().Count(expression);
        }
        public int Count()
        {
            return DataContext.Set<TEntity>().Count();
        }
        public void Dispose()
        {
            DataContext.Dispose();
        }
        //protected void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        DataContext.Dispose();
        //    }
        //}
    }
}