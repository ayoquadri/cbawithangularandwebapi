﻿using System.Linq;
using AutoMapper.QueryableExtensions;
using CBA.Core.DTOs;
using CBA.Core.Entities;
using CBA.Core.Persitence.Repositories;

namespace CBA.Persistence.Repositories
{
    //suppose to be internal
    public class BranchRespository : AbstractRepository<Branch,ApplicationDbContext>, IBranchRepository
    {
        public Branch GetBranchByName(string name)
        {
            return DataContext.Set<Branch>().FirstOrDefault(b => b.Name == name);
            //return Filter(b => b.Name == name).FirstOrDefault();
        }

        public IQueryable<BranchDto> GetQueryableBranches(string searchTerm = null)
        {
            if(!string.IsNullOrWhiteSpace(searchTerm))
            {
                return DataContext.Branches
                                    .Where(b => b.Name.Contains(searchTerm))
                                    .ProjectTo<BranchDto>();
            }
            return DataContext.Branches.ProjectTo<BranchDto>();
            //.Select(branch => new BranchDto
            //{
            //     Id = branch.Id,
            //     Address = branch.Address,
            //     IsBranchOpen = branch.IsBranchOpen,
            //     Name = branch.Name
            //});
        }
    }
}
