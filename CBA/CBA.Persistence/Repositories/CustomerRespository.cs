﻿using System.Linq;
using System.Threading.Tasks;
using AutoMapper.QueryableExtensions;
using CBA.Core.DTOs;
using CBA.Core.Entities;
using CBA.Core.Persitence.Repositories;

namespace CBA.Persistence.Repositories
{
    //suppose to be internal
    public class CustomerRespository : AbstractRepository<Customer, ApplicationDbContext>,ICustomerRepository
    {
        public IQueryable GetQueryableCustomers(string searchTerm = null)
        {
            if (!string.IsNullOrWhiteSpace(searchTerm))
            {
                return DataContext.Customers
                                    .Where(c => c.FirstName.Contains(searchTerm) || c.LastName.Contains(searchTerm)
                                                        || c.CustomerID.ToString().Equals(searchTerm)
                                                    || c.OtherNames.Contains(searchTerm) || c.PhoneNumber.Contains(searchTerm))
                                    .ProjectTo<CustomerDto>();
            }
            return GetAll().ProjectTo<CustomerDto>();
        }

        //public int GetLastCustomerID()
        //{
            
        //    return GetAll().Last().CustomerID;
        //}

        public async Task AddNewCustomer(Customer customer)
        {
            var lastCustomer = GetAll().OrderByDescending(x=>x.Id).FirstOrDefault();
            int lastCustomerId = lastCustomer?.CustomerID ?? 0;
            //TODO: This does not seem thread safe
            customer.CustomerID = ++lastCustomerId;
            await Save(customer);
        }

        public async Task<Customer> UpdateCustomer(int id,Customer entity)
        {
            var customer = await Get(id);
            if (customer == null) //Invalid id.. an id that do not exist in the database
                return null;
            entity.CustomerID = customer.CustomerID;
            DataContext.Entry(customer).CurrentValues.SetValues(entity);
            await DataContext.SaveChangesAsync();
            return entity;
        }

        public IQueryable<CustomerDto> FindCustomers(string search)
        {
            search = search.ToLower();
            return  DataContext.Set<Customer>().Where(c =>
                c.CustomerID.ToString().Contains(search) || c.FirstName.Contains(search) ||
                c.LastName.Contains(search)||
                c.OtherNames.Contains(search)
            ).ProjectTo<CustomerDto>();
        }
    }
}
